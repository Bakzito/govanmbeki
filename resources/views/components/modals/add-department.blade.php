<!-- Modal -->
<div class="modal fade" id="modal_add_new_department" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{route('department-create')}}" method="post" class="async">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <i class="fa fa-plus"></i>Add New Department
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container" style="width: 100%;">
                        <div class="form-group">
                            <label>Department REF</label>
                            <input class="form-control" type="text" name="ref" required>
                            <label>Name</label>
                            <input class="form-control" type="text" name="name"required>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input class="form-control" type="text" name="description" required>

                            <label>Contact</label>
                            <input class="form-control" type="text" name="contact" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> submit</button>
                </div>
            </div>
        </form>
    </div>
</div>


