<?php

namespace App\Repositories;

use Laravel5Helpers\Repositories\Search;

use App\Models\ReportSubscription as Model;


class ReportSubscription extends Search
{

    protected function getModel()
    {
        return new Model;
    }
}
