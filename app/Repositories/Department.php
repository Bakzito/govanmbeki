<?php

namespace App\Repositories;

use Laravel5Helpers\Repositories\Search;

use App\Models\Department as Model;


class Department extends Search
{

    protected function getModel()
    {
        return new Model;
    }

    public function updateDepartment($department)
    {
        $department_obj = Model::where('uuid', $department['uuid'])->first();

        $department_obj->ref = $department['ref'];
        $department_obj->name = $department['name'];
        $department_obj->description = $department['description'];
        $department_obj->contact = $department['contact'];
        $department_obj->save();
        return $department;
    }
}
