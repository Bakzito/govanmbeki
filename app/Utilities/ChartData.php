<?php

namespace App\Utilities;

use App\Models\Message;

class ChartData {

    public static function  getMonthlyOutgoingSms() {
        $data =[] ;
        $messages = Message::all();
        $years = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($years as $year){
            $yearTotal = 0;
            foreach($messages as $message) {
                if (substr($message->created_at, 5, 2) == $year){
                    $yearTotal ++;
                }
            }

            $data[] = $yearTotal;
        }

        return json_encode($data);
    }

    public static function  getMonthlyIncomingSms() {
        $data =[] ;
        $messages = Message::where('status', 'reply')->get();
        $years = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($years as $year){
            $yearTotal = 0;
            foreach($messages as $message) {
                if (substr($message->created_at, 5, 2) == $year){
                    $yearTotal ++;
                }
            }

            $data[] = $yearTotal;
        }

        return json_encode($data);
    }

    public static function getMonthlyFailedSms(){
        $data =[] ;
        $messages = Message::where('status', 'failed')->get();
        $years = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($years as $year){
            $yearTotal = 0;
            foreach($messages as $message) {
                if (substr($message->created_at, 5, 2) == $year){
                    $yearTotal ++;
                }
            }

            $data[] = $yearTotal;
        }

        return json_encode($data);
    }

}
