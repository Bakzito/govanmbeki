@extends('layouts.app')

@section('content')

    <section class="banner-section banner-one">

        <div class="banner-carousel owl-theme owl-carousel">

            <div class="slide-item">

                <div class="image-layer" style="background-image: url(theme/images/main-slider/3.jpg);"></div>

                <div class="auto-container">

                    <div class="content-box">

                        <div class="content clearfix">

                            <div class="inner">

                                <h1 style="text-transform: capitalize !important;">Govan Mbeki Municipality <br>Smart City<br></h1>

                                <div class="text">No# 1 In Service Delivery, Innovation and Good Governance.</div>

                                <div class="links-box clearfix">
                                    <a href="/smart-city" class="theme-btn btn-style-one"><span class="btn-title">Smart City</span></a>
{{--                                    <a href="https://www.stlm.gov.za/LED/Sequence%20.mp4" target="_blank" rel="nofollow" class="theme-btn btn-style-two lightbox-image"><span class="btn-title">--}}
{{--										<i aria-hidden="true" class="icon  flaticon-media-play-symbol"></i>	About Video</span></a>--}}
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="next-slide">

                    <div class="inner">

                        <div class="count">
                            <i class="fa fa-building"></i>
                        </div>

                        <div class="text">Bringing a Smarter City to You</div>

                        <div class="arrow"><span class="flaticon-next"></span></div>

                    </div>

                </div>

            </div>

            <!-- Slide Item -->

            <div class="slide-item">

                <div class="image-layer" style="background-image: url(theme/images/main-slider/2.jpg);"></div>

                <div class="auto-container">

                    <div class="content-box">

                        <div class="content clearfix">

                            <div class="inner">

                                <h1 style="text-transform: capitalize !important;">Govan Mbeki Municipality <br>Smart City<br></h1>

                                <div class="text">No# 1 In Service Delivery, Innovation and Good Governance.</div>

                                <div class="links-box clearfix">
                                    <a href="/smart-city" class="theme-btn btn-style-one"><span class="btn-title">Smart City</span></a>
{{--                                    <a href="https://www.stlm.gov.za/LED/Sequence%20.mp4" target="_blank" rel="nofollow" class="theme-btn btn-style-two lightbox-image"><span class="btn-title">--}}
{{--										<i aria-hidden="true" class="icon  flaticon-media-play-symbol"></i>									About Video</span></a>--}}
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="next-slide">

                    <div class="inner">

                        <div class="count">02</div>

                        <div class="text">Securely Connect to the City</div>

                        <div class="arrow"><span class="flaticon-next"></span></div>

                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection
