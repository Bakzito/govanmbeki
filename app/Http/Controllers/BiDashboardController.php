<?php

namespace App\Http\Controllers;

use App\Models\SourceSystem;
use App\Models\WorkspaceSettings;
use Illuminate\Contracts\View\View;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BiDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $workspace = WorkspaceSettings::where('user_uuid', Auth::user()->uuid)->first();
        $data = array(
            'workspace' => $workspace,
            'sliderReports' => explode(',', $workspace->reports ?? '')
        );

        return view('sub-systems.bi-dashboard.index', $data);
    }

    /**
     * Show Located Sub-system.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function subSystem($uuid)
    {
        try {
            $sourceSystem = SourceSystem::where('uuid', $uuid)->with('reports')->first();

            return view('sub-systems.bi-dashboard.source-system.index', ['system' => $sourceSystem->name, 'sourceSystem'=>$sourceSystem]);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception->getMessage(), false,false);
        }
    }

    /**
     * Show user Tools through iframe with specified link.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function myTools(Request $request)
    {
        $name = $request->get('name');
        $link = $request->get('link');
        if(!isset($name) || !isset($link)){
            return redirect()->back();
        }

        return view('my-tools.index', ['name'=>$name, 'link'=>$link]);
    }
}
