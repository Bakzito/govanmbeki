@extends('layouts.app')

@section('extra-css')
    <link href="/css/dashboard-global.css" rel="stylesheet">
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12" style="border: none; padding: 2%;">
            <div class="row clearfix">

                <!--Feature Block-->
                <div class="featured-block-three col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box"><span class="flaticon-briefcase"></span></div>
                            <div class="content">
                                <div class="subtitle">{{$departments->count()}}</div>
                                <h4><a href="/bi-dashboard/departments">Total number of Departments</a></h4>
                            </div>
                        </div>
                        <div class="hover-box show">
                            <div class="inner">
                                <h4><a href="/bi-dashboard/departments">{{$departments->count()}}</a></h4>
                                <div class="text">Total number of Departments</div>
                            </div>
                        </div>
                        <div class="more-link">
                            <a data-toggle="modal" data-target="#modal_add_new_department"><span
                                        class="fa fa-plus"></span></a>
                        </div>
                    </div>
                </div>

                <!--Feature Block-->
                <div class="featured-block-three col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box"><span class="flaticon-user"></span></div>
                            <div class="content">
                                <div class="subtitle">{{\App\User::all()->count()}}</div>
                                <h4><a href="/bi-dashboard/users">Total number of System Users</a></h4>
                            </div>
                        </div>
                        <div class="hover-box">
                            <div class="inner">
                                <h4><a href="/bi-dashboard/users">{{\App\User::all()->count()}}</a></h4>
                                <div class="text">Total number of System Users</div>
                            </div>
                        </div>
                        <div class="more-link">
                            <a href="/bi-dashboard/users"><span class="flaticon-right-2"></span></a>
                        </div>
                    </div>
                </div>

                <!--Feature Block-->
                <div class="featured-block-three col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box"><span class="flaticon-bar-chart"></span></div>
                            <div class="content">
                                <div class="subtitle">{{\App\User::all()->count()}}</div>
                                <h4><a href="/bi-dashboard/settings">System Logs</a></h4>
                            </div>
                        </div>
                        <div class="hover-box">
                            <div class="inner">
                                <h4><a href="/bi-dashboard/logs">{{\App\User::all()->count()}}</a></h4>
                                <div class="text">Total Report Subscriptions</div>
                            </div>
                        </div>
                        <div class="more-link">
                            <a href="/bi-dashboard/logs"><span class="flaticon-right-2"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-theme stat-left no-margin no-box-shadow">
                <div class="panel-body bg-theme" style="border: none;padding: 0% 2%;">
                    {{--                    <h3 class="text-center">STLM Departments</h3>--}}
                    <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                        <thead>
                        <tr>
                            <th style="width: 1%">Id</th>
                            <th style="width: 30%">Name</th>
                            <th style="width: 25%">Description</th>
                            <th style="width: 20%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($departments as $department)
                            <tr>
                                <td>{{$department->ref}}</td>
                                <td>{{$department->name}}</td>
                                <td>{{$department->description}}</td>
                                <td class="centered">
                                    <a class="btn mr-3" data-toggle="modal" data-target="#update_{{$department->uuid}}"><i
                                                class="fa fa-edit text-info"></i></a>
                                    <a href="/department/delete/{{$department->uuid}}" class="delete btn "> <i
                                                class="fa fa-trash" style="color: red"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-sm-8 -->
    </div>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                destroy: true,
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function (row) {
                                var data = row.data();
                                return 'Details for ' + data[0] + ' ' + data[1];
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                            tableClass: 'table'
                        })
                    }
                }
            });
        });
    </script>
@endsection
