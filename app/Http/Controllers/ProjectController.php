<?php

namespace App\Http\Controllers;


use App\Models\SourceSystem;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application departments dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function current()
    {
        $projects = SourceSystem::all();
        $users = User::all();
        $data = [
            'projects' => $projects,
            'users'=>$users
        ];
        return view('projects.current', $data);
    }

    private function getDepartmentDefinition($data)
    {
        return new \App\Definitions\SourceSystem($data);
    }

    private function getDepartmentRepository()
    {
        return new \App\Repositories\SourceSystem();
    }
}
