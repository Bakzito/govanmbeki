<?php

//
// Reports Routes
//
Route::get('/report/details/{uuid}', 'ReportController@update')->name('report-details');
Route::post('/report/create', 'ReportController@create')->name('report-create');
Route::delete('/report/delete/{uuid}', 'ReportController@delete')->name('report-delete');
Route::post('/report/update', 'ReportController@update')->name('report-update');
Route::get('/report/update/{uuid}/{key}/{value}', 'ReportController@update')->name('report-update-field-value');
Route::post('/report-subscription', 'ReportController@subscription')->name('report-update-field-value');
Route::delete('/report/subscription/delete/{uuid}', 'ReportController@deleteSubscription')->name('report-delete');

Route::post('/report/workspace-settings/create', 'SettingController@create')->name('create-or-update');

