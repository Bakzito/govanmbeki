<!-- KPI's -->
<div class="col-md-12">
    <!-- /.panel-heading -->
    <div class="clearfix"></div>
    <div id="tour-12" class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix rounded" style="background: rgba(0,0,0,0.5)">
                <a href="javascript:void(0)">
                                            <span class="mini-stat-icon" style="background: rgba(0,0,0,0.5);line-height: 50px;"><i
                                                        class="{{$kpi1_icon}} fg-light" style=";line-height: 50px;"></i></span>
                    <div class="mini-stat-info" style="color: #ffffff;">
                        <span class="">{{$kpi1_value}}</span>
                        {{$kpi1_label}}
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix rounded" style="background: rgba(0,0,0,0.5)">
                <a href="javascript:void(0)">
                                            <span class="mini-stat-icon" style="background: rgba(0,0,0,0.5)"><i
                                                        class="{{$kpi2_icon}} fg-light" style=";line-height: 50px;"></i></span>
                    <div class="mini-stat-info" style="color: #ffffff;">
                        <span class="">{{$kpi2_value}}</span>
                        {{$kpi2_label}}
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix rounded" style="background: rgba(0,0,0,0.5)">
                <a href="javascript:void(0)">
                                            <span class="mini-stat-icon" style="background: rgba(0,0,0,0.5)"><i
                                                        class="{{$kpi3_icon}}" style=";line-height: 50px;"></i></span>
                    <div class="mini-stat-info" style="color: #ffffff;">
                        <span class="">{{$kpi3_value}}</span>
                        {{$kpi3_label}}
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix rounded" style="background: rgba(0,0,0,0.5)">
                <a href="javascript:void(0)">
                                            <span class="mini-stat-icon" style="background: rgba(0,0,0,0.5)"><i
                                                        class="{{$kpi4_icon}} fg-light" style=";line-height: 50px;"></i></span>
                    <div class="mini-stat-info" style="color: #ffffff;">
                        <span class="">{{$kpi4_value}}</span>
                        {{$kpi4_label}}
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End KPI's -->