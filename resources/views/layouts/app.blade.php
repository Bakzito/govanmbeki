<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Steve Tshwete - Local Municipality</title>
    <!-- Global Stylesheet -->
    <link href="{{asset('css/global.css')}}" rel="stylesheet">
    <!-- Stylesheets -->
    <link href="{{asset('theme/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('theme/css/style.css')}}" rel="stylesheet">
    <!-- Responsive File -->
    <link href="{{asset('theme/css/responsive.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset('theme/images/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('theme/images/favicon.ico')}}" type="image/x-icon">
    <!-- Resonsive Settings -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js')}}"></script><![endif]-->
<!--[if lt IE 9]>
    <script src="{{asset('theme/js/respond.js')}}"></script><![endif]-->

    <!-- Dashboard Adapted Styles -->
    <link href="/theme-admin/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/animate.css/animate.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="/theme-admin/plugins/jquery.gritter/css/jquery.gritter.css" rel="stylesheet">
    <link href="/theme-admin/plugins/bootstrap-tour/bootstrap-tour.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/theme-admin/plugins/chartist/chartist.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/cube-portfolio/cubeportfolio.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <link href="/theme-admin/plugins/c3js-chart/c3.min.css" rel="stylesheet">
    <!--/ END PAGE LEVEL Styles -->

    <link href="/css/dashboard-global.css" rel="stylesheet">

    <!-- DATA TABLES CSS -->
    <link href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
        .search-main {
            font-size: 20px;
            padding: 11%;
            /* border-radius: 50%; */
            color: #aaaaaa;
            background-color: white;
        }
    </style>

    @yield('extra-css')

</head>

<body style="overflow: hidden;">

<div class="page-wrapper" style="height: -webkit-fill-available !important;background-color: white;">

    <!-- Preloader -->
    <div class="preloader">
        <div class="icon"></div>
    </div>

@include('components.header')

<!--Search Popup-->
    <div id="search-popup" class="search-popup">

        <div class="close-search theme-btn"><span class="flaticon-targeting-cross"></span></div>

        <div class="popup-inner">
            <div class="overlay-layer"></div>
            <div class="search-form">
                <form method="post" action="index.html">
                    <div class="form-group">
                        <fieldset>
                            <input type="search" class="form-control" name="search-input" value=""
                                   placeholder="Search Here" required>
                            <input type="submit" value="Search GMM" class="theme-btn">
                        </fieldset>
                    </div>
                </form>
                <br>
                <h3>Recent Search Keywords</h3>
                <ul class="recent-searches">

                    <li><a href="/smart-city#smart_city">Smart City</a></li>

                    <li><a href="/smart-city#Connectivity">Connectivity</a></li>

                    <li><a href="/smart-city#Public_Data">Public Data</a></li>

                    <li><a href="/smart-city#Public_Portals">Public Portals</a></li>

                </ul>


            </div>


        </div>

    </div>

    @yield('content')

</div>

<!--End pagewrapper-->

<!--Scroll to top-->

<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon flaticon-up-arrow-angle"></span></div>


<!-- GLOBAL MODALS -->
@include('components.modals.add-user')
@include('components.modals.add-project')
@include('components.modals.add-report', ['source_system_uuid'=>$sourceSystem->uuid ?? null])

@guest()

@else()
    @include('components.modals.workspace-settings')
@endguest

@foreach($sourceSystem->reports ?? [] as $report)
    @include('components.modals.report-subscription')
@endforeach

<!-- END GLOBAL MODALS -->

<script src="{{asset('theme/js/jquery.js')}}"></script>
<script src="{{asset('theme/js/popper.min.js')}}"></script>
<script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
<script src="{{asset('theme/js/jquery-ui.js')}}"></script>
<script src="{{asset('theme/js/jquery.fancybox.js')}}"></script>
<script src="{{asset('theme/js/owl.js')}}"></script>
<script src="{{asset('theme/js/scrollbar.js')}}"></script>
<script src="{{asset('theme/js/appear.js')}}"></script>
<script src="{{asset('theme/js/wow.js')}}"></script>
<script src="{{asset('theme/js/custom-script.js')}}"></script>

<!-- Dashboard Adapted Scripts -->
<script src="/theme-admin/plugins/jquery-cookie/jquery.cookie.js"></script>
{{--<script src="/theme-admin/plugins/bootstrap/bootstrap.min.js"></script>--}}

<script src="/theme-admin/plugins/typehead.js/handlebars.js"></script>
<script src="/theme-admin/plugins/typehead.js/typeahead.bundle.min.js"></script>
<script src="/theme-admin/plugins/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script src="/theme-admin/plugins/jquery.sparkline.min/index.js"></script>
<script src="/theme-admin/plugins/jquery-easing-original/jquery.easing.1.3.min.js"></script>
<script src="/theme-admin/plugins/ionsound/ion.sound.min.js"></script>
<script src="/theme-admin/plugins/bootbox/bootbox.js"></script>
<script src="/theme-admin/plugins/retina.js/retina.min.js"></script>

<script src="/theme-admin/plugins/bootstrap-session-timeout/bootstrap-session-timeout.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.spline.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.categories.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.resize.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.pie.js"></script>
<script src="/theme-admin/plugins/dropzone/dropzone.min.js"></script>
<script src="/theme-admin/plugins/jquery.gritter/js/jquery.gritter.min.js"></script>
<script src="/theme-admin/plugins/skycons-html5/skycons.js"></script>
<script src="/theme-admin/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="/theme-admin/plugins/counter-up/jquery.counterup.min.js"></script>
<script src="/theme-admin/plugins/bootstrap-tour/bootstrap-tour.min.js"></script>
<script src="/theme-admin/plugins/moment/moment.min.js"></script>
<script src="/theme-admin/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/theme-admin/plugins/chartist/chartist.min.js"></script>
<script src="/theme-admin/plugins/cube-portfolio/jquery.cubeportfolio.min.js"></script>
<script src="/theme-admin/plugins/d3/d3.min.js" charset="utf-8"></script>
<script src="/theme-admin/plugins/c3js-chart/c3.min.js"></script>
<script src="/theme-admin/plugins/flot/excanvas.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.time.js"></script>
<script src="/theme-admin/plugins/flot/jshashtable-2.1.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.symbol.js"></script>
<script src="/theme-admin/plugins/jquery-numberformatter/jquery.numberformatter-1.2.3.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.axislabels.js"></script>
<script src="/theme-admin/plugins/chartjs/Chart.min.js"></script>
<script src="/theme-admin/plugins/raphael/raphael-min.js"></script>
<script src="/theme-admin/plugins/morrisjs/morris.min.js"></script>
<!-- Dashboard Adapted Scripts END -->

<!-- DATA TABLES -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.8/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.8/js/responsive.bootstrap4.min.js"></script>

<!-- AJAX -->
<script type="text/javascript" src="{{asset('/vendors/premium-digital/ajax/toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/vendors/premium-digital/ajax/bootbox.js')}}"></script>
<script type="text/javascript" src="{{asset('/vendors/premium-digital/ajax/ajax-form.js')}}"></script>
<script type="text/javascript" src="{{asset('/vendors/premium-digital/ajax/main.js')}}"></script>

@yield('extra-js')

<script>
    var index = 0;

    function removeField(id) {
        $('#' + id).remove();
    }

    $(document).ready(function () {
        var index = 0;

        $('#add-field').click(function () {
            var control = '<input id="' + index + '" class="form-control" type="text" name="dm_fields[]" placeholder="{\'name\':\'text\'}" style="width: 180px;float: left;"><span style="float: left;"><i class="fa fa-times" onclick="removeField(index)"></i></span>';
            $('#fields_wrapper').append(control);
            index++;
        });


        $('.mdb-select').materialSelect();

        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        });
        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }

    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
</script>

</body>

@if(isset($users))
    @foreach($users as $user)
        @include('components.modals.update-user')
    @endforeach
@endif

@include('components.modals.add-department')

@if(isset($departments))
    @foreach($departments as $department)
        @include('components.modals.update-department')
    @endforeach
@endif

</html>
