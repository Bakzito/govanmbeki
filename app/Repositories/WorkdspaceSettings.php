<?php

namespace App\Repositories;

use Laravel5Helpers\Repositories\Search;

use App\Models\WorkspaceSettings as Model;

class WorkdspaceSettings extends Search
{

    protected function getModel()
    {
        return new Model;
    }
}
