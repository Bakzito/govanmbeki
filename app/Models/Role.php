<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class Role extends UuidModel
{
    protected $table = 'user_roles';

    public function user()
    {
        return $this->hasMany('App\Models\User', 'role_uuid', 'uuid');
    }
}
