<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class SourceSystem extends UuidModel
{
    protected $table = 'source_systems';

    public function dataMarts()
    {
        return $this->hasMany('App\Models\DataMart', 'source_systems_uuid', 'uuid');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report', 'source_systems_uuid', 'uuid');
    }


}
