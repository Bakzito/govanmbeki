<?php

namespace App\Repositories;

use Laravel5Helpers\Repositories\Search;

use App\Models\SourceSystem as Model;


class SourceSystem extends Search
{

    protected function getModel()
    {
        return new Model;
    }

    public static function getDataMarts($source_system_uuid)
    {
        return Model::where('source_system_uuid',$source_system_uuid)->get();
    }
}
