@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Department</label>
                            <div class="col-md-6">
                                <select id="department_uuid" type="text" class="form-control @error('department_uuid') is-invalid @enderror"
                                        name="department_uuid"
                                        value="{{ old('department_uuid') }}"
                                        required autocomplete="department_uuid"
                                        autofocus>

                                    <optgroup label="Municipality Departments">
                                        <option value="00">Finance</option>
                                        <option value="01">Internal Audit</option>
                                        <option value="02">Information Communication Technology</option>
                                        <option value="03">IDP</option>
                                        <option value="04">LED</option>
                                        <option value="05">Gender & Social Development</option>
                                        <option value="06">Youth Development and Recreation</option>
                                        <option value="07">Communication, Marketing, Branding and Media Relations</option>
                                        <option value="08">Human Resource Management</option>
                                        <option value="09">Legal & Administration</option>
                                        <option value="10">PMS</option>
                                        <option value="11">Property and Valuation Services</option>
                                        <option value="12">Physical Environmental Development</option>
                                        <option value="13">Cultural Services</option>
                                        <option value="14">Emergency Services</option>
                                        <option value="15"> Licensing Department</option>
                                        <option value="16">Traffic & Law Enforcement</option>
                                        <option value="17">Electricity Services</option>
                                        <option value="18">Municipal Building Services</option>
                                        <option value="19">Roads & Storm Water</option>
                                        <option value="20">Sanitation</option>
                                        <option value="21">Town Planning</option>
                                        <option value="22">Environmental and Solid Waste Management</option>
                                        <option value="23">Water Services</option>
                                    </optgroup>

                                </select>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <input id="uuid" type="hidden" value="{{ \Ramsey\Uuid\Uuid::uuid4() }}">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
