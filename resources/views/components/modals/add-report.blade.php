<!-- Modal -->
<div class="modal fade" id="modal_add_new_report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-plus"></i> Add New Report
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container" style="width: 100%;">
                    <div class="row">
                        <section style="width: 100%;">
                            <div class="">
                                <form class="async" action="/report/create" method="post" role="form" id="report_create">
                                    @csrf
                                    <div class="tab-content">
                                        <div class="tab-pane active" role="tabpanel" id="step1">
                                            <h3>1. Report Details</h3>
                                            <p>This is the Details of a report you want to add from the BI Tool.</p>
                                            <div class="form-group">
                                                <label>Report Title</label>
                                                <input class="form-control" type="text" name="title">
                                            </div>
                                            <div class="form-group">
                                                <label>Report Description</label>
                                                <input class="form-control" type="text" name="description">
                                            </div>
                                            <div class="form-group">
                                                <label>Select Department</label>
                                                <select class="form-control" name="department_uuid">
                                                    @foreach(\App\Models\Department::all() as $department)
                                                    <option value="{{$department->uuid}}">{{$department->name}}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Embed Type</label>
                                                <select class="form-control" name="embed_type">
                                                    <option value="iframe">iframe</option>
                                                    <option value="iframe">secured-iframe</option>
                                                    <option value="link" disabled>link</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Embed String (iframe / Power Bi Link)</label>
                                                <input class="form-control" type="text" name="embed">
                                            </div>
                                            <div class="form-group">
                                                <label>Refresh Rate</label>
                                                <select class="form-control" name="refresh_rate">
                                                    <option value="1_hours">Every Hour <small>(from point of setting)</small></option>
                                                    <option value="12_hours">Every 12 Hours</option>
                                                    <option value="24_hours">Every 24 Hours</option>
                                                    <option value="1_weeks">Every Week <small>(every Sunday evening)</small></option>
                                                    <option value="2_weeks">Every 2 Weeks</option>
                                                    <option value="3_weeks">Every 3 Weeks</option>
                                                    <option value="1_month">Every Month <small>(1st of every Month)</small></option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Publish ?</label>
                                                <select class="form-control" name="published">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>
                                            <div class="form-group"><br></div>
                                            <ul class="list-inline pull-right">
                                                <input type="hidden" name="source_systems_uuid" value="{{$source_system_uuid}}">
                                                <li>
                                                    <button type="submit" class="btn btn-primary next-step">
                                                        Save
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('extra-js')
    <script>
        function addMoreFields(){
            var control = '<input class="form-control" type="text" name="dm_fields[]" placeholder="{\'name\':\'text\'}" style="width: 180px;float: left;">';
            $('#fields_wrapper').append(control);
        }
    </script>
@endsection
