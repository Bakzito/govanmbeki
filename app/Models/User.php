<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class User extends UuidModel
{
    protected $table = 'users';
    protected $fillable=[
        'role_uuid',
        'department_uuid',
        'employee_no',
        'position',
        'name',
        'email',
        'contact',
        'digital_signature',


    ];
    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'department_uuid', 'uuid');
    }
}
