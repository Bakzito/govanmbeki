$(document).ajaxStop(function () {
    //$('#loading').modal({dismissible: true});
    $(".preloader").fadeOut(300);
    $('.helper_loader').modal('hide');
});

$(document).ajaxStart(function () {
    //$('#loading').modal();
    $(".preloader").fadeIn(300);
});

$(function () {
    $('[data-toggle="popover"]').popover();
});

$(document).ready(function () {
    $('#info').bind('DOMNodeInserted', function (event) {
        $('#info').slideDown().delay(3500).slideUp(500, function () {
            $('#info').empty();
        });
    });

    $('.toggle-next').click(function (event) {
        var next = $(this).next();

        if ($(next).hasClass('sub-nav')) {
            event.defaultPrevented();
            event.preventDefault();
        }

        $(this).next().slideToggle();
    });
    var inputs = document.querySelectorAll('.custom-file-upload');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });
});

// basic ajax form submit
$(document).on('submit', 'form.async', function (event) {
    event.preventDefault();
    var form = $(this);
    var target = $(form.data('target'));
    if (target.length < 1) {
        target = $('#info');
    }

    $("#overlay").fadeIn(300);
    // document.getElementById("loader").style.display = "block";
    $(this).ajaxSubmit({
        type: form.attr('method'),
        success: function (data) {
            $(target).html(data.message);
            var toggleOnSuccess = $(form.data('toggle'));
            $(toggleOnSuccess).toggle();
            // document.getElementById("loader").style.display = "none";
            toastr.success(data.message, 'Success', {timeOut: 5000})
        },
        error: function (data) {
            var result = JSON.parse(data.responseText);
            $(target).prepend(result.message);
            // document.getElementById("loader").style.display = "none";
            toastr.error(result.message, 'Error', {timeOut: 5000})
        },
        complete: function (data) {
            var result = JSON.parse(data.responseText);
            if (result.reload) {
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }

            if (result.redirect) {
                setTimeout(function () {
                    location.href = result.redirect;
                }, 2000);
            }
        },
        uploadProgress: function (event, position, total, percentComplete) {
            $('#file-upload-progress').show().attr('value', percentComplete);
        }
    })
});


// ajax form submit with view response
$(document).on('submit', 'form.async-ajax', function (event) {
    event.preventDefault();
    var me = $(this);
    var html_id = me.data('html');
    var form = $(this);
    var target = $(form.data('target'));
    if (target.length < 1) {
        target = $('#info');
    }

    $("#overlay").fadeIn(300);
    $(this).ajaxSubmit({
        type: form.attr('method'),
        success: function (data) {
            $(target).html(data.message);
            var toggleOnSuccess = $(form.data('toggle'));
            $(toggleOnSuccess).toggle();
            //toastr.success(data.message, 'Success', {timeOut: 5000})
            $('#' + html_id).html(data.message);
        },
        error: function (data) {
            var result = JSON.parse(data.responseText);
            $(target).prepend(result.message);
            toastr.error(result.message, 'Error', {timeOut: 5000})
        },
        complete: function (data) {
            var result = JSON.parse(data.responseText);
            if (result.reload) {
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }

            if (result.redirect) {
                setTimeout(function () {
                    location.href = result.redirect;
                }, 2000);
            }
        },
        uploadProgress: function (event, position, total, percentComplete) {
            $('#file-upload-progress').show().attr('value', percentComplete);
        }
    })
});

//
jQuery(function ($) {

    // Function which adds the 'animated' class to any '.animatable' in view
    var doAnimations = function () {

        // Calc current offset and get all animatables
        var offset = $(window).scrollTop() + $(window).height(),
            $animatables = $('.animatable');

        // Unbind scroll handler if we have no animatables
        if ($animatables.length == 0) {
            $(window).off('scroll', doAnimations);
        }

        // Check all animatables and animate them if necessary
        $animatables.each(function (i) {
            var $animatable = $(this);
            if (($animatable.offset().top + $animatable.height() - 20) < offset) {
                $animatable.removeClass('animatable').addClass('animated');
            }
        });

    };

    // Hook doAnimations on scroll, and trigger a scroll
    $(window).on('scroll', doAnimations);
    $(window).trigger('scroll');

});

$(document).on('click', 'a.async', function (event) {
    event.preventDefault();
    var link = $(this);
    var target = $(link.attr('data-target'));
    if (target.length < 1) {
        target = $('#info');
    }

    $("#overlay").fadeIn(300);
    $.ajax({
        type: 'GET',
        url: link.attr('href'),
        cache: true,
        timeout: 60000,
        success: function (data, status) {
            $(target).html(data.message);
            toastr.success(data.message, 'Success', {timeOut: 5000})
        },
        error: function (data) {
            var result = JSON.parse(data.responseText);
            $(target).prepend(result.message);
            toastr.error(result.message, 'Error', {timeOut: 5000})
        }, complete: function (data) {
            var result = JSON.parse(data.responseText);
            if (result.reload) {
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }

            if (result.redirect) {
                setTimeout(function () {
                    location.href = result.redirect;
                }, 2000);
            }
        }
    });
    return false;
});

// page load
function pageLoad(url, wrapper) {
    var token = $('input[name=_token]').val();
    $('#' + wrapper).html(getLoaderHtml());
    $('#'.loading).show();
    setTimeout(
        $.ajax({
            type: 'GET',
            url: getUrlWithParams(url, {_token: token}),
            cache: true,
            timeout: 60000,
            success: function (data, status) {
                $('#' + wrapper).html(data.message);
                $('#'.loading).hide();
            },
            error: function (data) {
                var result = JSON.parse(data.responseText);
                $('#'.loading).hide();
                $('#' + wrapper).html(result.message);
            }, complete: function (data) {
                var result = JSON.parse(data.responseText);
                if (result.reload) {
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        location.href = result.redirect;
                    }, 2000);
                }
            }
        }), 3000)
}

$(document).on('click', '.async-check', function (event) {
    event.preventDefault();
    var link = $(this);
    var target = $(link.attr('data-target'));
    if (target.length < 1) {
        target = $('#info');
    }

    if (this.checked) {
        $("#overlay").fadeIn(300);
        $.ajax({
            type: 'GET',
            url: link.data('href'),
            cache: true,
            timeout: 60000,
            success: function (data, status) {
                $(target).html(data.message);
            },
            error: function (data) {
                var result = JSON.parse(data.responseText);
                $(target).prepend(result.message);
            }, complete: function (data) {
                var result = JSON.parse(data.responseText);
                if (result.reload) {
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }

                if (result.redirect) {
                    setTimeout(function () {
                        location.href = result.redirect;
                    }, 2000);
                }
            }
        });
        return false;
    }
});

$(document).on('click', '.delete', function (event) {
    event.preventDefault();
    var token = $('input[name=_token]').val();
    var me = $(this);

    bootbox.confirm({
        message: 'You will not be able to recover this record.',
        closeButton: false,
        callback: function (result) {
            if (result === true) {
                $("#helper_loader").fadeIn(300);
                $.ajax({
                    type: 'DELETE',
                    url: getUrlWithParams($(me).attr('href'), {_token: token}),
                    cache: false,
                    timeout: 60000,
                    success: function (data, status) {
                        $('#info').html(data.message);
                        $(me).closest('tr').remove();
                        toastr.success(data.message, 'Success', {timeOut: 5000})
                    },
                    error: function (data) {
                        var result = JSON.parse(data.responseText);
                        $('#info').prepend(result.message);
                        toastr.error(result.message, 'Error', {timeOut: 5000})
                    },
                    complete: function (data) {
                        bootbox.hideAll();
                        var result = JSON.parse(data.responseText);
                        if (result.reload) {
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }

                        if (result.redirect) {
                            setTimeout(function () {
                                location.href = result.redirect;
                            }, 2000);
                        }
                    }
                });
                return false;
            }
        },
        title: 'Are you sure?'
    });
});


$(document).on('click', 'a.ask-confirmation', function (event) {
    event.preventDefault();
    var token = $('input[name=_token]').val();
    var me = $(this);

    bootbox.confirm({
        message: me.data('message'),
        closeButton: false,
        callback: function (result) {
            if (result === true) {
                $("#helper_loader").fadeIn(300);
                $.ajax({
                    type: 'GET',
                    url: getUrlWithParams($(me).attr('href'), {_token: token}),
                    cache: false,
                    timeout: 60000,
                    success: function (data, status) {
                        $('#info').html(data.message);
                        $(me).closest('tr').remove();
                        toastr.success(data.message, 'Success', {timeOut: 5000})
                    },
                    error: function (data) {
                        var result = JSON.parse(data.responseText);
                        $('#info').prepend(result.message);
                        toastr.error(result.message, 'Error', {timeOut: 5000})
                    },
                    complete: function (data) {
                        bootbox.hideAll();
                        var result = JSON.parse(data.responseText);
                        if (result.reload) {
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }

                        if (result.redirect) {
                            setTimeout(function () {
                                location.href = result.redirect;
                            }, 2000);
                        }
                    }
                });
                return false;
            }
        },
        title: 'Are you sure?'
    });
});

var getUrlWithParams = function (url, data) {
    if (!$.isEmptyObject(data)) {
        url += (url.indexOf('?') >= 0 ? '&' : '?') + $.param(data);
    }

    return url;
};

/* Google Maps */


// AGENT SEARCH FUNCTION
var btsearch = {
    init: function (search_field, searchable_elements, searchable_text_class) {
        $(search_field).keyup(function (e) {
            e.preventDefault();
            var query = $(this).val().toLowerCase();
            if (query) {
                // loop through all elements to find match
                $.each($(searchable_elements), function () {
                    var title = $(this).find(searchable_text_class).text().toLowerCase();
                    if (title.indexOf(query) == -1) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
            } else {
                // empty query so show everything
                $(searchable_elements).show();
            }
        });
    }
}

// INIT
$(function () {
    // USAGE: btsearch.init(('search field element', 'searchable children elements', 'searchable text class');
    btsearch.init('#search_field', '#agents div', '#agent-name');
});

function tran(n) {
    if (n == 1) {

        $("#slider_inner").slideUp('slow', 'swing');
        $("#agent-list").fadeOut(500);
        $("#package-search").fadeIn(800);
        $('#slider_wrapper').css('padding-bottom', '300px',
            'transition', 'all 0.3s ease-in-out',
            '-webkit-transition', 'all 0.7s ease-in-out',
            '-moz-transition', 'all 0.7s ease-in-out',
            '-o-transition', 'all 0.7s ease-in-out',);

    }

    if (n == 2) {
        $("#slider_inner").slideUp('slow', 'swing');
        $(".packages").fadeOut(500);
        $("#agent-list").fadeIn(800);
        $('#slider_wrapper').css('padding-bottom', '300px',
            'transition', 'all 0.3s ease-in-out',
            '-webkit-transition', 'all 0.7s ease-in-out',
            '-moz-transition', 'all 0.7s ease-in-out',
            '-o-transition', 'all 0.7s ease-in-out',);
    }


}


// $(document).ready(function () {
//         $('.btn-form1-submit').on('click',function(){
//             $("#slider_inner").slideUp('slow', 'swing');
//             $("#agent-list").fadeOut(500);
//             $("#package-search").fadeIn(800);
//             $('#slider_wrapper').css('padding-bottom', '300px',
//             'transition', 'all 0.3s ease-in-out',
//             '-webkit-transition', 'all 0.7s ease-in-out',
//             '-moz-transition', 'all 0.7s ease-in-out',
//             '-o-transition', 'all 0.7s ease-in-out',);

//         });
//     });
