<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application departments dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'departments' => Department::all()
        ];
        return view('departments.index', $data);
    }

    public function create(Request $request)
    {
        try {
            $attributes = $request->all();
            $definition = $this->getDepartmentDefinition($attributes);
            $department = $this->getDepartmentRepository()->createResource($definition);
            return $this->ajaxSuccess('Successfully Added', false, true);

        } catch (QueryException $e) {

        }
    }

    public function update(Request $request)
    {
        try {
            $attributes = $request->all();
            $definition = $this->getDepartmentDefinition($attributes);
            $department = $this->getDepartmentRepository()->updateDepartment($attributes);
            return $this->ajaxSuccess('Successfully Added', false, true);
        } catch (QueryException $e) {

        }
    }

    public function delete($id)
    {
        try {
            $this->getDepartmentRepository()->deleteResource($id);
            return $this->ajaxSuccess('Successfully Added', false, true);

        } catch (QueryException $e) {

        }
    }

    private function getDepartmentDefinition($data)
    {
        return new \App\Definitions\Department($data);
    }

    private function getDepartmentRepository()
    {
        return new \App\Repositories\Department();
    }
}
