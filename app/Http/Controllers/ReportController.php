<?php

namespace App\Http\Controllers;

use App\Definitions\Report;
use App\Definitions\ReportSubscription;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application departments dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        try {
            $attributes = $request->all();
            $definition = $this->getReportDefinition($attributes);
            $report = $this->getReportRepository()->createResource($definition);

            return $this->ajaxSuccess('Successfully Added', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception->getMessage(), false,false);
        }
    }

    public function delete($uuid)
    {
        try {
            $this->getReportRepository()->deleteResource($uuid);

            return $this->ajaxSuccess('Successfully Deleted', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception->getMessage(), false,false);
        }
    }

    public function deleteSubscription($uuid)
    {
        try {
            $this->getReportSubscriptionRepository()->deleteResource($uuid);

            return $this->ajaxSuccess('Successfully Deleted', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception->getMessage(), false,false);
        }
    }

    public function subscription(Request $request)
    {
        try {
            $attributes = $request->all();
            foreach ($attributes['user_uuid'] as $user_uuid){
                $attributes['user_uuid'] = $user_uuid;
                $subscription = \App\Models\ReportSubscription::where('user_uuid',$user_uuid)->where('report_uuid',$attributes['report_uuid'])->first();

                if(!isset($subscription->uuid)){
                    $definition = $this->getReportSubscriptionDefinition($attributes);
                    $reportSubscription = $this->getReportSubscriptionRepository()->createResource($definition);
                }
            }

            return $this->ajaxSuccess('Successfully Added', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception->getMessage(), false,false);
        }
    }

    public function update($uuid, $key,$value)
    {
        try {
            \App\Models\Report::where('uuid',$uuid)->update([
                $key => $value
            ]);

            return $this->ajaxSuccess('Successfully Updated', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception->getMessage(), false,false);
        }
    }

    private function getReportDefinition($data){
        return new Report($data);
    }

    private function getReportRepository(){
        return new \App\Repositories\Report();
    }

    private function getReportSubscriptionDefinition($data){
        return new ReportSubscription($data);
    }

    private function getReportSubscriptionRepository(){
        return new \App\Repositories\ReportSubscription();
    }
}
