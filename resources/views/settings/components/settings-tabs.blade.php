<div class="row row-merge">
    <div class="col-sm-12 col-md-12">
        <br>
        <h3 class="text-center">{{ ucwords($sourceSystem->name) }} Reports</h3>
        <div class="panel panel-theme stat-left no-margin no-box-shadow" style="padding: 1%">
            <table id="datatable_source_systems" class="table table-striped table-bordered" style="width:100%;">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Department</th>
                    <th>Embed <br> Type</th>
                    <th>Refresh <br> Rate</th>
                    <th>Published</th>
                    <th>
                        <a href="/source-system/delete/{{$sourceSystem->uuid}}" class="delete" style="color: red">
                            <i class="fa fa-times-circle"></i>
                            <small>delete system</small>
                        </a>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($sourceSystem->reports as $report)
                    <tr>
                        <td>{{$report->id}}</td>
                        <td>{{$report->title}}</td>
                        <td>{{$report->description}}</td>
                        <td>{{$report->department->name ?? 'n/a'}}</td>
                        <td>{{$report->embed_type}}</td>
                        <td>{{$report->refresh_rate}}</td>
                        <td>{{$report->published==1?'Yes':'No'}}</td>
                        <td>
                            <div class="dropdown show">
                                <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Options
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a style="color: #0b2e13" class="dropdown-item" href="{{$report->embed}}" target="_blank">
                                        Test Link
                                    </a>
                                    <br>
                                    @php $publishToggle = $report->published==1?0:1@endphp
                                    <a style="color: #0b2e13" class="dropdown-item async" href="/report/update/{{$report->uuid}}/published/{{$publishToggle}}" target="_blank">
                                        Publish / Unpublish
                                    </a>
                                    <br>
                                    <a style="color: #0b2e13" class="dropdown-item async" href="#" data-toggle="modal" data-target="#modal_report_subscription_{{$report->uuid}}">
                                        Report Subscription
                                    </a>
                                    <br>
                                    <a style="color: #0b2e13"  class="dropdown-item delete" href="/report/delete/{{$report->uuid}}">
                                        Delete
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.panel -->
    </div><!-- /.col-sm-8 -->
</div>
<!-- /.row -->
