<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class Report extends UuidModel
{
    protected $table = 'reports';

    public function sourceSystem()
    {
        return $this->belongsTo('App\Models\SourceSystem', 'source_system_uuid', 'uuid');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'department_uuid', 'uuid');
    }
}
