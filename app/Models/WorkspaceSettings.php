<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class WorkspaceSettings extends UuidModel
{
    protected $table = 'workspace_settings';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_uuid', 'uuid');
    }
}
