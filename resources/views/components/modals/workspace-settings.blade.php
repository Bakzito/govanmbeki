<!-- Modal -->
<div class="modal fade" id="modal_workspace_settings" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-user"></i> &nbsp; My Workspace Settings
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container" style="width: 100%;">
                    <div class="row">
                        <div class="col-md-12">
                            @php $worspaceSettings = \App\Models\WorkspaceSettings::where('user_uuid', \Illuminate\Support\Facades\Auth::user()->uuid)->first() @endphp
                            <form class="async" action="/report/workspace-settings/create" method="post" role="form">
                                @csrf
                                <div class="tab-content">
                                    <div class="tab-pane active" role="tabpanel" id="step1">
                                        <div class="form-group">
                                            <label><b>Activate Reports Slider Dashboard ?</b></label>
                                            <select class="form-control" name="slider">
                                                <option value="NO" @if(isset($worspaceSettings->slider) && str_contains($worspaceSettings->slider, 'NO')) selected @endif>No</option>
                                                <option value="YES" @if(isset($worspaceSettings->slider) && str_contains($worspaceSettings->slider, 'YES')) selected @endif>Yes</option>
                                            </select>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label><b>Select Dashboard Reports</b></label><br>

                                            @foreach(\App\Models\Report::all() as $report)
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox"
                                                           id="inlineCheckbox{{$report->id}}" value="{{$report->uuid}}"
                                                           @if(isset($worspaceSettings->reports) && str_contains($worspaceSettings->reports, $report->uuid)) checked @endif
                                                           name="reports[]"/>
                                                    <label class="form-check-label"
                                                           for="inlineCheckbox{{$report->id}}">{{$report->title}}</label>
                                                </div>
                                                <br>
                                            @endforeach

                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label><b>Slider Timer</b></label>
                                            <select class="form-control" name="slider_timer">
                                                <option value="99999999999">Never</option>
                                                <option value="30000">Every 30 Second</option>
                                                <option value="60000">Every 60 Second</option>
                                                <option value="300000">Every 5 Minutes</option>
                                                <option value="600000">Every 10 Minutes</option>
                                                <option value="1800000">Every 30 Minutes</option>
                                                <option value="3600000">Every Hour</option>
                                            </select>
                                        </div>
                                        <hr>
                                        @if(\Illuminate\Support\Facades\Auth::user()->role_uuid == 'ADMIN')
                                            <input type="hidden" name="department_uuid" value="ADMIN">
                                            <div class="form-group">
                                                <label><b>Flash / Broadcast Message</b></label>
                                                <input class="form-control" type="text"
                                                       @if(isset($worspaceSettings->flash_message)) value="{{$worspaceSettings->flash_message}}" @endif
                                                       name="flash_message">
                                            </div>
                                            <hr>
                                        @else
                                            <input type="hidden" name="flash_message" value="{{$worspaceSettings->flash_message}}">
                                        @endif
                                        <div class="form-group"><br></div>
                                        <ul class="list-inline pull-right">
                                            <input type="hidden" name="user_uuid"
                                                   value="{{\Illuminate\Support\Facades\Auth::user()->uuid}}">
                                            <li>
                                                <button type="submit" class="btn btn-primary next-step">
                                                    Save
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
