<?php

//
// Source System Routes
//
Route::get('/source-system/details/{uuid}', 'SourceSystemController@update')->name('source-system-update');
Route::post('/source-system/create', 'SourceSystemController@create')->name('source-system-create');
Route::delete('/source-system/delete/{uuid}', 'SourceSystemController@delete')->name('source-system-delete');
Route::post('/source-system/update', 'SourceSystemController@update')->name('source-system-update');

//
// Source System Reports Routes
//
Route::post('/source-system/report/create', 'ReportController@create')->name('report-create');
Route::delete('/source-system/report/delete/{uuid}', 'ReportController@delete')->name('report-delete');
Route::post('/source-system/report/update', 'ReportController@update')->name('report-update');
