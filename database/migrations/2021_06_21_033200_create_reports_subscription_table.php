<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('user_uuid')->nullable();
            $table->string('department_uuid')->nullable();
            $table->string('unit_uuid')->nullable();
            $table->string('report_uuid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_subscription');
    }
}
