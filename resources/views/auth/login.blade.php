@extends('layouts.app')

@section('extra-css')
<style>
    .login_panel_bottom_right{
        width: 400px;
        float: right;
        margin: 0% 1%;
        background-color: #0000008a !important;
        display: block;
        zoom: 1.2;
        position: fixed;
        bottom: 2%;
        padding: 2%;
        right: 0%;
    }
</style>
    @endsection

@section('content')

    <section class="banner-section banner-one">
        <div class="banner-carousel owl-theme owl-carousel">

            <!-- Slide Item -->
            <div class="slide-item">
                <div class="image-layer" style="background-image: url(/theme/images/pexels-photo-1587097.jpeg);
                 -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                    background-size: cover;
                "></div>
                <div class="auto-container">
                    <div class="content-box">
                        <div class="content clearfix">
                            <div class="inner">
                                <h1 style="text-transform: capitalize !important;color: #ffffff;">Govan Mbeki Municipality <br>BI Dashboard
                                </h1>
                                <div class="text" style="color: #ffffff;">Smart City Project 2021.</div>
                                <div class="links-box clearfix">
{{--                                    <a href="/smart-city" class="theme-btn btn-style-one"><span class="btn-title"><i--}}
{{--                                                    class="fa fa-angle-left"></i> Back</span></a>--}}
{{--                                    <a href="" class="theme-btn btn-style-two" data-toggle="modal"--}}
{{--                                       data-target="#modalLoginForm"><span class="btn-title">Login--}}
{{--										<i aria-hidden="true" class="fa fa-angle-right"></i></span>--}}
{{--                                    </a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="login_panel_bottom_right">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                       value="{{ old('email') }}" required autocomplete="email"  placeholder="Email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message ?? '' }}</strong></span>
                                @enderror
                                <br>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                       name="password" required autocomplete="current-password" placeholder="Password">
                                @error('password')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="pull-right theme-btn btn-style-two" style="height: 100%;background-color: #0000005e;">
                                    <span class="btn-title" style="    background-color: #0000002b;color: white;zoom: 1.4;">
                                            <i aria-hidden="true" class="fa fa-angle-right"></i>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>

@endsection
