<?php

namespace App\Utilities;

use Adldap\Laravel\Facades\Adldap;

class Ldap
{
    public static function index()
    {
        // Get all users:
        return Adldap::search()->users()->get();
    }

    public static function findUser($value)
    {
        // Finding a user:
        return Adldap::search()->users()->find($value);
    }

    public static function searchUser($whereKey,$whereValue,$whereOperator='=')
    {
        // Searching for a user:
        return Adldap::search()->where($whereKey, $whereOperator, $whereValue)->get();
    }

    public static function createUser()
    {
//        return Adldap::make()->user([
//            'cn' => 'John Doe',
//        ]);
    }

}
