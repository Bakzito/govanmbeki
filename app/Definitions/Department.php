<?php

namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;
use Illuminate\Support\Str;

class Department extends Definition
{
    public $uuid;

    public $name;

    public $ref;

    public $description;

    public $contact;


    public function __construct($data)
    {
        parent::__construct($data);
    }

    protected function setValidators()
    {
        return $this->validators = [
        ];
    }
}
