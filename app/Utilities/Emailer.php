<?php

namespace App\Utilities;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class Emailer
{

    private $to;
    private $from;
    private $cc;

    public function __construct($to, $from, $cc = '')
    {
        $this->to = $to;
        $this->from = $from;
        if ($cc) {
            $this->cc = $cc;
        }
    }

    public static function sendEMail($params)
    {
        $to = "somebody@example.com, somebodyelse@example.com";
        $subject = "HTML email";

        $message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>This email contains HTML Tags!</p>
<table>
<tr>
<th>Firstname</th>
<th>Lastname</th>
</tr>
<tr>
<td>John</td>
<td>Doe</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
        $headers .= 'From: <webmaster@example.com>' . "\r\n";
        $headers .= 'Cc: myboss@example.com' . "\r\n";

        mail($to, $subject, $message, $headers);
    }

    public static function sendHtmlEmail($params, $template = "emails.access_codze")
    {
        Mail::send(["html" => $template], $params, function ($message) use ($params) {
            $message->to($params['email'])
                ->from(Auth::user()->email)
                ->replyTo(Auth::user()->email)
                ->subject($params['subject']);
        });
    }

    public function send($subject, $message)
    {
        try {
            if (!mail($this->to, $subject, $message, $this->getHeaders())) {
                throw new \Exception('Error sending email.');
            }
            return $this;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    private function getHeaders()
    {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <' . $this->from . '>' . "\r\n";
        $headers .= 'Cc:' . $this->cc . "\r\n";

        return $headers;
    }

}
