# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.18)
# Database: stlm_smartcity
# Generation Time: 2021-06-20 10:42:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table data_marts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_marts`;

CREATE TABLE `data_marts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_systems_uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fields` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_cleaning_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_sync_frequency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_sync_day` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_sync_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_marts` WRITE;
/*!40000 ALTER TABLE `data_marts` DISABLE KEYS */;

INSERT INTO `data_marts` (`id`, `uuid`, `source_systems_uuid`, `name`, `fields`, `status`, `data_cleaning_method`, `data_sync_frequency`, `data_sync_day`, `data_sync_time`, `created_at`, `updated_at`)
VALUES
	(1,'b518adec-ac44-49b4-97ef-ab6ae73d9e91','f3b652ff-9840-4a0a-b07b-f46eeeb2f942','\"Lane Norton\"','[\"Quaerat in voluptate\",null,null,null,null,null,null]',NULL,'Default','Weekly','Sunday','03:34','2021-06-18 22:00:40','2021-06-18 22:00:40'),
	(2,'5f707bae-99e3-4a38-9111-a3a30ed8f339','57dfc9f4-d947-46f7-9b6d-961ee74299c3','\"Josephine Hopkins\"','[\"Enim non praesentium\"]',NULL,'Default','Monthly','Thursday','09:12','2021-06-18 22:09:32','2021-06-18 22:09:32');

/*!40000 ALTER TABLE `data_marts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table departments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `departments_ref_unique` (`ref`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;

INSERT INTO `departments` (`id`, `uuid`, `ref`, `name`, `description`, `contact`, `created_at`, `updated_at`)
VALUES
	(1,'1','FIN','Finance','Finance','0',NULL,NULL),
	(2,'2','HR','Human Resource','Human Resource','0',NULL,NULL),
	(3,'3','CS','Corporate Services','Corporate Services','0',NULL,NULL),
	(4,'4','SS','Social Services','Social Services','0',NULL,NULL);

/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2019_08_19_000000_create_failed_jobs_table',1),
	(4,'2021_06_05_170212_create_user_roles_tables',1),
	(5,'2021_06_05_170228_create_departments_table',1),
	(6,'2021_06_18_153236_create_source_systems_table',2),
	(8,'2021_06_18_153317_create_data_marts_table',3),
	(11,'2021_06_18_230130_create_reports_table',4);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table monitor_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_config`;

CREATE TABLE `monitor_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `monitor_config` WRITE;
/*!40000 ALTER TABLE `monitor_config` DISABLE KEYS */;

INSERT INTO `monitor_config` (`key`, `value`)
VALUES
	('language','en_US'),
	('proxy','0'),
	('proxy_url',''),
	('proxy_user',''),
	('proxy_password',''),
	('email_status','1'),
	('email_add_url','0'),
	('email_from_email','monitor@example.org'),
	('email_from_name','Server Monitor'),
	('email_smtp',''),
	('email_smtp_host',''),
	('email_smtp_port',''),
	('email_smtp_security',''),
	('email_smtp_username',''),
	('email_smtp_password',''),
	('sms_status','0'),
	('sms_gateway','messagebird'),
	('sms_gateway_username','username'),
	('sms_gateway_password','password'),
	('sms_from','1234567890'),
	('webhook_status','0'),
	('pushover_status','0'),
	('pushover_api_token',''),
	('telegram_status','0'),
	('telegram_add_url','0'),
	('telegram_api_token',''),
	('jabber_status','1'),
	('jabber_host',''),
	('jabber_port',''),
	('jabber_username',''),
	('jabber_domain',''),
	('jabber_password',''),
	('password_encrypt_key','f7d19f45eca14c3fd54c2438bb7aaad1319a4d25'),
	('alert_type','status'),
	('log_status','1'),
	('log_email','1'),
	('log_sms','1'),
	('log_pushover','1'),
	('log_webhook','1'),
	('log_telegram','1'),
	('log_jabber','1'),
	('discord_status','0'),
	('log_jdiscord','1'),
	('log_retention_period','365'),
	('version','3.6.0.beta2'),
	('version_update_check','3.5.2'),
	('auto_refresh_servers','0'),
	('show_update','1'),
	('last_update_check','1624169718'),
	('cron_running','0'),
	('cron_running_time','0'),
	('cron_off_running','0'),
	('cron_off_running_time','0');

/*!40000 ALTER TABLE `monitor_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table monitor_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_log`;

CREATE TABLE `monitor_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) unsigned NOT NULL,
  `type` enum('status','email','sms','discord','pushover','webhook','telegram','jabber') NOT NULL,
  `message` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `monitor_log` WRITE;
/*!40000 ALTER TABLE `monitor_log` DISABLE KEYS */;

INSERT INTO `monitor_log` (`log_id`, `server_id`, `type`, `message`, `datetime`)
VALUES
	(1,4,'status','Server \'STLM Data Warehouse Teradata SQL Server\' is DOWN: ip=197.242.150.181, port=3306. Error=Connection refused','2021-06-20 08:27:55'),
	(2,5,'status','Server \'STLM Data Warehouse LDAP Service\' is DOWN: ip=197.242.150.181, port=389. Error=Operation timed out','2021-06-20 08:28:15'),
	(3,6,'status','Server \'STLM Data Warehouse FTP Service\' is DOWN: ip=197.242.150.181, port=0. Error=Failed to parse address \"197.242.150.181\"','2021-06-20 08:28:15');

/*!40000 ALTER TABLE `monitor_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table monitor_log_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_log_users`;

CREATE TABLE `monitor_log_users` (
  `log_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`log_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table monitor_servers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_servers`;

CREATE TABLE `monitor_servers` (
  `server_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(500) NOT NULL,
  `port` int(5) NOT NULL,
  `request_method` varchar(50) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `type` enum('ping','service','website') NOT NULL DEFAULT 'service',
  `pattern` varchar(255) NOT NULL DEFAULT '',
  `pattern_online` enum('yes','no') NOT NULL DEFAULT 'yes',
  `post_field` varchar(255) DEFAULT NULL,
  `redirect_check` enum('ok','bad') NOT NULL DEFAULT 'bad',
  `allow_http_status` varchar(255) NOT NULL DEFAULT '',
  `header_name` varchar(255) NOT NULL DEFAULT '',
  `header_value` varchar(255) NOT NULL DEFAULT '',
  `status` enum('on','off') NOT NULL DEFAULT 'on',
  `error` varchar(255) DEFAULT NULL,
  `rtime` float(9,7) DEFAULT NULL,
  `last_online` datetime DEFAULT NULL,
  `last_offline` datetime DEFAULT NULL,
  `last_offline_duration` varchar(255) DEFAULT NULL,
  `last_check` datetime DEFAULT NULL,
  `active` enum('yes','no') NOT NULL DEFAULT 'yes',
  `email` enum('yes','no') NOT NULL DEFAULT 'yes',
  `sms` enum('yes','no') NOT NULL DEFAULT 'no',
  `discord` enum('yes','no') NOT NULL DEFAULT 'yes',
  `pushover` enum('yes','no') NOT NULL DEFAULT 'yes',
  `webhook` enum('yes','no') NOT NULL DEFAULT 'yes',
  `telegram` enum('yes','no') NOT NULL DEFAULT 'yes',
  `jabber` enum('yes','no') NOT NULL DEFAULT 'yes',
  `warning_threshold` mediumint(1) unsigned NOT NULL DEFAULT '1',
  `warning_threshold_counter` mediumint(1) unsigned NOT NULL DEFAULT '0',
  `ssl_cert_expiry_days` mediumint(1) unsigned NOT NULL DEFAULT '0',
  `ssl_cert_expired_time` varchar(255) DEFAULT NULL,
  `timeout` smallint(1) unsigned DEFAULT NULL,
  `website_username` varchar(255) DEFAULT NULL,
  `website_password` varchar(255) DEFAULT NULL,
  `last_error` varchar(255) DEFAULT NULL,
  `last_error_output` text,
  `last_output` text,
  PRIMARY KEY (`server_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

LOCK TABLES `monitor_servers` WRITE;
/*!40000 ALTER TABLE `monitor_servers` DISABLE KEYS */;

INSERT INTO `monitor_servers` (`server_id`, `ip`, `port`, `request_method`, `label`, `type`, `pattern`, `pattern_online`, `post_field`, `redirect_check`, `allow_http_status`, `header_name`, `header_value`, `status`, `error`, `rtime`, `last_online`, `last_offline`, `last_offline_duration`, `last_check`, `active`, `email`, `sms`, `discord`, `pushover`, `webhook`, `telegram`, `jabber`, `warning_threshold`, `warning_threshold_counter`, `ssl_cert_expiry_days`, `ssl_cert_expired_time`, `timeout`, `website_username`, `website_password`, `last_error`, `last_error_output`, `last_output`)
VALUES
	(1,'http://sourceforge.net/index.php',80,NULL,'SourceForge','website','','yes',NULL,'bad','','','','on','',2.9025769,'2021-06-20 06:27:55',NULL,NULL,'2021-06-20 06:27:55','yes','yes','yes','yes','yes','yes','yes','yes',1,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'HTTP/1.1 301 Moved Permanently\r\nServer: nginx\r\nDate: Sun, 20 Jun 2021 06:27:53 GMT\r\nContent-Type: text/html\r\nContent-Length: 178\r\nConnection: keep-alive\r\nLocation: https://sourceforge.net/index.php\r\n\r\nHTTP/2 301 \r\nserver: nginx\r\ndate: Sun, 20 Jun 2021 06:27:55 GMT\r\ncontent-type: text/html; charset=UTF-8\r\nlocation: https://sourceforge.net/\r\ncache-control: no-cache\r\npragma: no-cache\r\nx-ua-compatible: IE=edge,chrome=1\r\nx-frame-options: SAMEORIGIN\r\ncontent-security-policy: frame-ancestors \'self\', upgrade-insecure-requests\r\nset-cookie: VISITOR=00d70c2c-b1e1-4795-8630-2e0fcbff90d3; Max-Age=315360000; Path=/; expires=Wed, 18-Jun-2031 06:27:51 GMT; secure; HttpOnly\r\nx-content-type-options: nosniff\r\nstrict-transport-security: max-age=31536000\r\n\r\nHTTP/2 200 \r\nserver: nginx\r\ndate: Sun, 20 Jun 2021 06:27:55 GMT\r\ncontent-type: text/html; charset=utf-8\r\ncache-control: no-cache\r\npragma: no-cache\r\nx-ua-compatible: IE=edge,chrome=1\r\nx-frame-options: SAMEORIGIN\r\ncontent-security-policy: frame-ancestors \'self\', upgrade-insecure-requests\r\nset-cookie: VISITOR=00d70c2c-b1e1-4795-8630-2e0fcbff90d3; Max-Age=315360000; Path=/; expires=Wed, 18-Jun-2031 06:27:55 GMT; secure; HttpOnly\r\nx-content-type-options: nosniff\r\nstrict-transport-security: max-age=31536000\r\ncontent-encoding: gzip\r\n\r\n'),
	(2,'smtp.gmail.com',465,NULL,'Gmail SMTP','service','','yes',NULL,'bad','','','','on','',0.2195358,'2021-06-20 06:27:55',NULL,NULL,'2021-06-20 06:27:55','yes','yes','yes','yes','yes','yes','yes','yes',1,0,0,NULL,NULL,NULL,NULL,NULL,NULL,''),
	(3,'197.242.150.181',22,NULL,'STLM Data Warehouse Access (SSH)','service','','yes',NULL,'ok','','','','on','',0.0287080,'2021-06-20 06:27:55',NULL,NULL,'2021-06-20 06:27:55','yes','yes','no','yes','yes','yes','yes','yes',1,0,0,NULL,10,'','',NULL,NULL,''),
	(4,'197.242.150.181',3306,NULL,'STLM Data Warehouse Teradata SQL Server','service','','yes',NULL,'ok','','','','off','Connection refused',0.0438211,NULL,'2021-06-20 06:27:55',NULL,'2021-06-20 06:27:55','yes','yes','yes','yes','yes','yes','yes','yes',1,1,0,NULL,10,'','','Connection refused','Could not get headers. probably HTTP 50x error.',NULL),
	(5,'197.242.150.181',389,NULL,'STLM Data Warehouse LDAP Service','service','','yes',NULL,'ok','','','','off','Operation timed out',10.0351782,NULL,'2021-06-20 06:28:15',NULL,'2021-06-20 06:28:15','yes','yes','yes','yes','yes','yes','yes','yes',1,1,0,NULL,10,'','','Operation timed out','Could not get headers. probably HTTP 50x error.',NULL),
	(6,'197.242.150.181',0,NULL,'STLM Data Warehouse FTP Service','service','','yes',NULL,'ok','','','','off','Failed to parse address \"197.242.150.181\"',0.0000060,NULL,'2021-06-20 06:28:15',NULL,'2021-06-20 06:28:15','yes','yes','yes','yes','yes','yes','yes','yes',1,1,0,NULL,10,'','','Failed to parse address \"197.242.150.181\"','Could not get headers. probably HTTP 50x error.',NULL);

/*!40000 ALTER TABLE `monitor_servers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table monitor_servers_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_servers_history`;

CREATE TABLE `monitor_servers_history` (
  `servers_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) unsigned NOT NULL,
  `date` date NOT NULL,
  `latency_min` float(9,7) NOT NULL,
  `latency_avg` float(9,7) NOT NULL,
  `latency_max` float(9,7) NOT NULL,
  `checks_total` int(11) unsigned NOT NULL,
  `checks_failed` int(11) unsigned NOT NULL,
  PRIMARY KEY (`servers_history_id`),
  UNIQUE KEY `server_id_date` (`server_id`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table monitor_servers_uptime
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_servers_uptime`;

CREATE TABLE `monitor_servers_uptime` (
  `servers_uptime_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `latency` float(9,7) DEFAULT NULL,
  PRIMARY KEY (`servers_uptime_id`),
  KEY `server_id` (`server_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

LOCK TABLES `monitor_servers_uptime` WRITE;
/*!40000 ALTER TABLE `monitor_servers_uptime` DISABLE KEYS */;

INSERT INTO `monitor_servers_uptime` (`servers_uptime_id`, `server_id`, `date`, `status`, `latency`)
VALUES
	(1,1,'2021-06-20 06:16:09',1,2.8104851),
	(2,2,'2021-06-20 06:16:09',1,0.2244461),
	(3,1,'2021-06-20 06:20:36',1,2.6470602),
	(4,2,'2021-06-20 06:20:37',1,0.2115469),
	(5,3,'2021-06-20 06:20:37',1,0.0417819),
	(6,1,'2021-06-20 06:27:55',1,2.9025769),
	(7,2,'2021-06-20 06:27:55',1,0.2195358),
	(8,3,'2021-06-20 06:27:55',1,0.0287080),
	(9,4,'2021-06-20 06:27:55',0,0.0438211),
	(10,5,'2021-06-20 06:28:15',0,10.0351782),
	(11,6,'2021-06-20 06:28:15',0,0.0000060);

/*!40000 ALTER TABLE `monitor_servers_uptime` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table monitor_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_users`;

CREATE TABLE `monitor_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) NOT NULL COMMENT 'user''s name, unique',
  `password` varchar(255) NOT NULL COMMENT 'user''s password in salted and hashed format',
  `password_reset_hash` char(40) DEFAULT NULL COMMENT 'user''s password reset code',
  `password_reset_timestamp` bigint(20) DEFAULT NULL COMMENT 'timestamp of the password reset request',
  `rememberme_token` varchar(64) DEFAULT NULL COMMENT 'user''s remember-me cookie token',
  `level` tinyint(2) unsigned NOT NULL DEFAULT '20',
  `name` varchar(255) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `discord` varchar(255) NOT NULL,
  `pushover_key` varchar(255) NOT NULL,
  `pushover_device` varchar(255) NOT NULL,
  `webhook_url` varchar(255) NOT NULL,
  `webhook_json` varchar(255) NOT NULL DEFAULT '{"text":"servermon: #message"}',
  `telegram_id` varchar(255) NOT NULL,
  `jabber` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `unique_username` (`user_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `monitor_users` WRITE;
/*!40000 ALTER TABLE `monitor_users` DISABLE KEYS */;

INSERT INTO `monitor_users` (`user_id`, `user_name`, `password`, `password_reset_hash`, `password_reset_timestamp`, `rememberme_token`, `level`, `name`, `mobile`, `discord`, `pushover_key`, `pushover_device`, `webhook_url`, `webhook_json`, `telegram_id`, `jabber`, `email`)
VALUES
	(1,'Admin','$2y$10$/aRaB7hf6AktVkJpHh09lO6CGuAPyW1sf8v.eRvDLfbdPfzwjPdFa',NULL,NULL,NULL,10,'Admin','','','','','','','','','admin@stlm.gov.za');

/*!40000 ALTER TABLE `monitor_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table monitor_users_preferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_users_preferences`;

CREATE TABLE `monitor_users_preferences` (
  `user_id` int(11) unsigned NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `monitor_users_preferences` WRITE;
/*!40000 ALTER TABLE `monitor_users_preferences` DISABLE KEYS */;

INSERT INTO `monitor_users_preferences` (`user_id`, `key`, `value`)
VALUES
	(1,'status_layout','0');

/*!40000 ALTER TABLE `monitor_users_preferences` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table monitor_users_servers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_users_servers`;

CREATE TABLE `monitor_users_servers` (
  `user_id` int(11) unsigned NOT NULL,
  `server_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`server_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `monitor_users_servers` WRITE;
/*!40000 ALTER TABLE `monitor_users_servers` DISABLE KEYS */;

INSERT INTO `monitor_users_servers` (`user_id`, `server_id`)
VALUES
	(1,1),
	(1,2);

/*!40000 ALTER TABLE `monitor_users_servers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table reports
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_systems_uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `embed_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'iframe',
  `embed` text COLLATE utf8mb4_unicode_ci,
  `refresh_rate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'daily',
  `published` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;

INSERT INTO `reports` (`id`, `uuid`, `source_systems_uuid`, `department_uuid`, `name`, `title`, `description`, `embed_type`, `embed`, `refresh_rate`, `published`, `created_at`, `updated_at`)
VALUES
	(2,'c7378248-4455-4303-ba72-ef223eca6511','71164608-34d4-4f7b-9053-24d173e4ead9','1',NULL,'Balance Report','Munsoft Balance Report','iframe','https://app.powerbi.com/reportEmbed?reportId=abb8ff8f-e0ff-4674-9dee-de975990f3f7&autoAuth=true&ctid=f8d52c4c-5234-4e25-a29f-8fd347a01f5e&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXNvdXRoLWFmcmljYS1ub3J0aC1hLXByaW1hcnktcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D','12_hours','1','2021-06-20 05:06:27','2021-06-20 05:06:27'),
	(3,'3f73cdbe-dab0-4896-9b3c-211dec18cc5b','71164608-34d4-4f7b-9053-24d173e4ead9','1',NULL,'Financial Budgets Extract','Munsoft Financial Budgets Extract','iframe','https://app.powerbi.com/reportEmbed?reportId=303ab002-11db-45e4-8bdc-b7679df57660&autoAuth=true&ctid=f8d52c4c-5234-4e25-a29f-8fd347a01f5e&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXNvdXRoLWFmcmljYS1ub3J0aC1hLXByaW1hcnktcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D','12_hours','1','2021-06-20 05:07:51','2021-06-20 05:07:51'),
	(4,'181e51e6-0650-43db-b323-388f09ba7ddf','57dfc9f4-d947-46f7-9b6d-961ee74299c3','2',NULL,'Consequat Inventore','Quia labore saepe qu','iframe','Labore culpa nisi q','12_hours','1','2021-06-20 06:35:24','2021-06-20 06:35:24');

/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table source_systems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `source_systems`;

CREATE TABLE `source_systems` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `integration_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `source_systems` WRITE;
/*!40000 ALTER TABLE `source_systems` DISABLE KEYS */;

INSERT INTO `source_systems` (`id`, `uuid`, `name`, `description`, `ip`, `domain`, `integration_type`, `data_path`, `created_at`, `updated_at`)
VALUES
	(13,'71164608-34d4-4f7b-9053-24d173e4ead9','Munsoft',NULL,'0','/','FILE_UPLOAD','munsoft','2021-06-18 17:09:58','2021-06-18 17:09:58'),
	(16,'c9118446-e50f-40c6-81b9-44be2cc3cad6','IAMS',NULL,'0','/','FTP','iams','2021-06-18 21:46:03','2021-06-18 21:46:03'),
	(17,'f3b652ff-9840-4a0a-b07b-f46eeeb2f942','Property Valuation Service',NULL,'0','/','FTP','property_valuation_service','2021-06-18 22:00:40','2021-06-18 22:00:40'),
	(18,'57dfc9f4-d947-46f7-9b6d-961ee74299c3','Public Engagement',NULL,'0','/','FILE_UPLOAD','public_engagement','2021-06-18 22:09:32','2021-06-18 22:09:32');

/*!40000 ALTER TABLE `source_systems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_roles_ref_unique` (`ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `digital_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `uuid`, `role_uuid`, `department_uuid`, `employee_no`, `position`, `name`, `surname`, `email`, `contact`, `digital_signature`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'2515aeab-0533-407b-af40-663f447aadab',NULL,'00',NULL,NULL,'CFO',NULL,'cfo@stlm.gov.za',NULL,NULL,'$2y$10$ijGCOuuX8.I8FUBrE5fyqONxtTUO3HPIg4plF3iZkMFu1iE3Ikpg.','f3IPX6Cc97cog5WYDlTnhESclIN1N3mdvvsIa0u5LvYyMQ4uPI2d0mLOePP2','2021-06-05 17:30:37','2021-06-05 17:30:37'),
	(2,'006b183f-85bc-471b-9efc-7ba3c6f312fb','ADMIN','01',NULL,'','Admin',NULL,'admin@stlm.gov.za',NULL,NULL,'$2y$10$whISXf48827q/echcI.JxuQ68SgzdEXcPgvtVzfbSpH08wP9nalfe','hfKskpPccdPiwG3eEaq33lfYX4Rq8gK3CzX5tFP1Emn46huFgYAS8UvEGJeU','2021-06-08 06:43:02','2021-06-08 06:43:02');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
