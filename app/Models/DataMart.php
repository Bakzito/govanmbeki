<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class DataMart extends UuidModel
{
    protected $table = 'data_marts';

    public function sourceSystem()
    {
        return $this->belongsTo('App\Models\SourceSystem', 'source_systems_uuid', 'uuid');
    }
}
