@extends('layouts.app')

@section('extra-css')
    <style>
        .login_control{
            background-color: #fbfbfb2b;
            font-size: 28px;
            height: 50px;
            color: white;
        }
        .login_panel_docked{
            width: 20px !important;
            bottom: -15% !important;
        }
        .login_panel_open{
            width: 100% !important;
            bottom: 0% !important;
        }
    </style>
    @endsection

@section('content')

    <section class="banner-section banner-one">

        <div class="banner-carousel owl-theme owl-carousel">

            <!-- Slide Item -->

            <div class="slide-item">

                <div class="image-layer" style="background-image: url(/theme/images/smart-4168483_1920.jpg);
                 -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                    background-size: cover;
                "></div>

                <div class="auto-container">

                    <div class="content-box">

                        <div class="content clearfix">

                            <div class="inner">

                                <h1 style="text-transform: capitalize !important;color: #ffffff;">STLM <br>Public Engagement</h1>

                                <div class="text" style="color: #ffffff;">Smart City Project 2021.</div>

                                <div class="links-box clearfix">
                                    <a href="/smart-city" class="theme-btn btn-style-one"><span class="btn-title"><i class="fa fa-angle-left"></i> Back</span></a>
                                    <a href="https://pe.stlm-smartcity.online/"  class="theme-btn btn-style-two"><span class="btn-title">Continue
										<i aria-hidden="true" class="fa fa-angle-right"></i></span>
                                    </a>
                                </div>

                            </div>
                            {{-- <div class="next-slide" style="bottom: 20% !important;right: 5% !important;background-color: transparent;">
                                                <img src="{{asset('/theme/images/info.png')}}" style="width: 700px">
                                            </div> --}}
                        </div>

                    </div>

                </div>

               

                {{-- <div class="next-slide login_panel_docked" id="pe_login_toggle">

                    <div class="inner">

                        <div class="count">
                            <i class="fa fa-user" onclick="toggleLogin()"></i>
                        </div>

                        <div class="text">
                            <div class="form-group">
                                <input type="text" name="username" placeholder="email" required="" class="login_control">
                            </div>
                            <div class="form-group">
                                <input type="password" name="username" placeholder="password" required="" class="login_control">
                            </div>
                        </div>

                        <div class="arrow">
                            <a href="https://steve-tshwete.irsglobal.net/steve-tshwete/"><span class="flaticon-next"></span></a>
                        </div>

                    </div>

                </div> --}}

            </div>


        </div>

    </section>

@endsection

@section('extra-js')
<script>
    function toggleLogin(){
        console.log('toggling classes');
        $('#pe_login_toggle').toggleClass("login_panel_open login_panel_docked");
    }
</script>
@endsection