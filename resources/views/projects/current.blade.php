@extends('layouts.app')

@section('extra-css')
    <link href="/css/dashboard-global.css" rel="stylesheet">
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12" style="border: none; padding: 2%;">
            <h2 class="text-center">Current Projects <span class="pull-right"><a href="#" data-target="#modal_add_new_project" data-toggle="modal"><i class="fa fa-plus"></i></a> </span></h2>
        </div>
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-theme stat-left no-margin no-box-shadow">
                <div class="panel-body bg-theme" style="border: none;padding: 0% 2%;">
                    <table id="datatable_current_projects" class="table table-striped table-bordered nowrap" style="width:100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Domain / IP</th>
                            <th>Number of reports</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{{$project->id}}</td>
                                <td>{{$project->name}}</td>
                                <td>{{$project->ip}}</td>
                                <td>{{sizeof($project->reports)}}</td>
                                <td class="centered">
                                    <a href="/sub-system/bi-dashboard/{{$project->uuid}}" class="btn-secondary">
                                        <img border="0" alt="Go to Report" src="{{asset('img/PowerBIGraphic.png')}}" width="100" height="100">
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-sm-8 -->
    </div>

@endsection

@section('extra-js')
    <script>
        $(document).ready(function() {
            $('#datatable_current_projects').DataTable({
                destroy: true,
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function (row) {
                                var data = row.data();
                                return 'Details for ' + data[0] + ' ' + data[1];
                            }
                        }),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                            tableClass: 'table'
                        })
                    }
                }
            });
        } );

    </script>
@endsection

