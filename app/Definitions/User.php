<?php

namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;
use Illuminate\Support\Str;

class User extends Definition
{
    public $uuid;

    public $department_uuid;

    public $role_uuid;

    public $name;

    public $surname;

    public $email ;

    public $contact;

    public $digital_signature;

    public $password;

    public $employee_no;

    public $position;

    public function __construct($data)
    {
        parent::__construct($data);
    }

    protected function setValidators()
    {
        return $this->validators = [
            'name'        => 'required'
        ];
    }
}
