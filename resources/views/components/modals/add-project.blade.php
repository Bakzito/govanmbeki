<!-- Modal -->
<div class="modal fade" id="modal_add_new_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-plus"></i> Project On-Boarding / Add New Project
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container" style="width: 100%;">
                    <div class="row">
                        <section style="width: 100%;">
                            <div class="">
                                <div class="wizard-inner">
                                    {{--                                    <div class="connecting-line"></div>--}}
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="collection-tab" data-toggle="tab" href="#collection"
                                               role="tab" aria-controls="collection" aria-selected="false">
                                                <span class="tab">
                                                    <i class="fa fa-check"></i>
{{--                                                    Data Ingestion--}} Data Collection
                                                </span>
                                            </a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile"
                                               role="tab" aria-controls="profile" aria-selected="false">
                                                <span class="tab">
                                                    <i class="fa fa-database"></i>
{{--                                                    Data Warehouse Details--}} Data Storage
                                                </span>
                                            </a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact"
                                               role="tab" aria-controls="contact" aria-selected="false">
                                                <span class="tab">
                                                    <i class="fa fa-check"></i>
{{--                                                    Data Ingestion--}} Complete
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <form class="async" action="/source-system/create" method="post" role="form"
                                      id="form_source_system_setup">
                                    @csrf

                                    <div class="tab-content" id="myTabContent">

                                        <div class="tab-pane fade show active" id="collection" role="tabpanel"
                                             aria-labelledby="collection-tab">
                                            <br>
                                            <h2 class="text-center">1. Data Collection</h2>
                                            <p><br></p>
                                            <div class="form-group">
                                                <label>Project Name</label>
                                                <input class="form-control" type="text" name="name">
                                            </div>
                                            <div class="form-group">
                                                <label>Select Integration Type</label>
                                                <select class="form-control" name="integration_type">
                                                    <option value="REST_API">API (REST)</option>
                                                    <option value="FTP">FTP</option>
                                                    <option value="FILE_UPLOAD">FILE UPLOAD (CSV, JSON, EXCEL)</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>IP Address</label>
                                                <input class="form-control" type="text" name="ip">
                                            </div>
                                            <div class="form-group">
                                                <label>Domain <small>(if API or FTP)</small></label>
                                                <input class="form-control" type="text" name="domain">
                                            </div>
                                            <div class="form-group">
                                                <label>Project Owner (Name)</label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label>Data Folder Name <small>(for source system to dump data /
                                                        data-files)</small></label>
                                                <input class="form-control" type="text" name="data_path"
                                                       placeholder="i.e. use project name and system name">
                                            </div>
                                            <div class="form-group"><br></div>
                                            <div class="form-group"><br></div>
                                        </div>


                                        <div class="tab-pane fade" id="profile" role="tabpanel"
                                             aria-labelledby="profile-tab">
                                            <br>
                                            <h2 class="text-center">2. Data Store</h2>
                                            <p><br></p>

                                            <div class="form-group">
                                                <label>Select Data Warehouse</label>
                                                <select class="form-control" name="">
                                                    <option>192.168.8.190 - STLM Data Warehouse</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Give Your Data Mart a Name</label>
                                                <input class="form-control" type="text" name="dm_name">
                                            </div>
                                            <div class="form-group">

                                            </div>
                                            <div class="form-group">
                                                <label style="width: 100%">Sample Report</label> <br>
                                                <div class="wizard-inner">
                                                    {{--                                    <div class="connecting-line"></div>--}}
                                                    <ul class="nav nav-tabs" id="data_fields" role="tablist">

                                                        <li class="nav-item" role="presentation">
                                                            <a class="nav-link" id="manual-fields-tab"
                                                               data-toggle="tab"
                                                               href="#manual-fields" role="tab"
                                                               aria-controls="manual-fields"
                                                               aria-selected="false">
                                                                <span class="tab">
                                                                    <i class="fa fa-check"></i>
                {{--                                                    Data Ingestion--}}Manual
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" role="presentation">
                                                            <a class="nav-link" id="csv-fields-tab"
                                                               data-toggle="tab"
                                                               href="#csv-fields" role="tab"
                                                               aria-controls="csv-fields"
                                                               aria-selected="false">
                                                                <span class="tab">
                                                                    <i class="fa fa-database"></i>
                {{--                                                    Data Warehouse Details--}} Upload CSV / Excel File
                                                                </span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="tab-content">

                                                    <div class="tab-pane fade show active" id="manual-fields"
                                                         role="tabpanel"
                                                         aria-labelledby="manual-fields">
                                                        <button type="button" class="btn btn-primary"
                                                                style="float: left;" id="add-field"
                                                        >
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                        <br><br>
                                                        <span id="fields_wrapper"
                                                              style="width: 100%;    display: inline-block;"
                                                              class="pt-3">
                                                    <input class="form-control" type="text" name="dm_fields[]"
                                                           placeholder="{'name':'text'}"
                                                           style="width: 180px;float: left;">
                                                </span>
                                                    </div>
                                                    <div class="tab-pane fade show " id="csv-fields"
                                                         role="tabpanel"
                                                         aria-labelledby="csv-fields">
                                                        <input type="file" name="import_file" class="form-control"/>
                                                    </div>
                                                    <div class="form-group"><br></div>
                                                    <div class="form-group"><br></div>
                                                </div>
                                                <div class="tab-pane fade text-center" id="contact" role="tabpanel"
                                                     aria-labelledby="contact-tab">
                                                    <div class="form-group"><br></div>
                                                    <button type="button"
                                                            onclick="$('#form_source_system_setup').submit();"
                                                            class="btn btn-primary btn-info-full next-step">
                                                        Save System Settings
                                                    </button>
                                                    <div class="form-group"><br></div>
                                                    <div class="form-group"><br></div>
                                                </div>
                                            </div>

                                </form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('extra-js')
    <script>
        var index = 0;

        $('#add-field').click(function () {
            var control = '<input id="' + index + '" class="form-control" type="text" name="dm_fields[]" placeholder="{\'name\':\'text\'}" style="width: 180px;float: left;"><span style="float: left;"><i class="fa fa-times" onclick="removeField(index)"></i></span>';
            $('#fields_wrapper').append(control);
            index++;
        });


        function addMoreFields() {

        }

        function removeField(id) {
            $('#' + id).remove();
        }
    </script>
@endsection
