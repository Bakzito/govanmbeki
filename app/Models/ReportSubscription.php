<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class ReportSubscription extends UuidModel
{
    protected $table = 'reports_subscriptions';

    public function users()
    {
        return $this->hasMany('App\Models\User', 'role_uuid', 'uuid');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report', 'report_uuid', 'uuid');
    }

    public function departments()
    {
        return $this->hasMany('App\Models\Department', 'report_uuid', 'uuid');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_uuid', 'uuid');
    }

    public function report()
    {
        return $this->belongsTo('App\Models\Report', 'report_uuid', 'uuid');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'report_uuid', 'uuid');
    }
}
