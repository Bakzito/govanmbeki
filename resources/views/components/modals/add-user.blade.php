<!-- Modal -->
<div class="modal fade" id="modal_add_new_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-plus"></i> Add New User
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container" style="width: 100%;">
                    <div class="row">
                        <section style="width: 100%;">
                            <form class="async" action="/user/create" method="post" role="form" id="form_add_user">
                                @csrf

                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <br>
                                        <h2 class="text-center">Add New User Details</h2>
                                        <p><br></p>
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" type="text" name="name">
                                        </div>
                                        <div class="form-group">
                                            <label>Surname</label>
                                            <input class="form-control" type="text" name="surname">
                                        </div>
                                        <div class="form-group">
                                            <label>Department</label>
                                            <select class="form-control" name="department_uuid">
                                                @foreach(\App\Models\Department::all() as $department)
                                                <option value="{{$department->uuid}}">{{$department->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Role</label>
                                            <select class="form-control" name="role_uuid">
                                                @foreach(\App\Models\Role::all() as $role)
                                                    <option value="{{$role->uuid}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Position</label>
                                            <input class="form-control" type="text" name="position">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" type="email" name="email">
                                        </div>
                                        <div class="form-group">
                                            <label>Contact</label>
                                            <input class="form-control" type="text" name="contact">
                                        </div>
                                        <div class="form-group">
                                            <hr>
                                            <input class="form-control" type="hidden" name="password" value="{{'Stlm2021'}}">
                                        <button type="button"  onclick="$('#form_add_user').submit();"
                                                class="btn btn-primary btn-info-full next-step pull-right">
                                            Save Details
                                        </button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('extra-js')
    <script>
        function addMoreFields(){
            var control = '<input class="form-control" type="text" name="dm_fields[]" placeholder="{\'name\':\'text\'}" style="width: 180px;float: left;">';
            $('#fields_wrapper').append(control);
        }
    </script>
@endsection
