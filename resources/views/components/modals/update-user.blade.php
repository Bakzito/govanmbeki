<!-- Modal -->
    <div class="app-container modal fade" id="update_{{$user->uuid}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <i class="fa fa-plus"></i> Update User
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container" style="width: 100%;">
                        <div class="row">
                            <section style="width: 100%;">
                                <form action="{{route("user-update")}}" method="post"  class="async">
                                    @csrf
                                    <input class="form-control" type="hidden" name="user_id" value="{{$user->uuid}}">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel"
                                             aria-labelledby="home-tab">
                                            <br>
                                            <h2 class="text-center">Update User Details</h2>
                                            <p><br></p>
                                            <div class="form-group">
                                                <label>Name</label>
                                                        <input class="form-control" type="text" name="name" value="{{$user->name}}">
                                            </div>
                                            <div class="form-group">
                                                <label>Surname</label>
                                                <input class="form-control" type="text" name="surname" value="{{$user->surname}}">
                                            </div>
                                            <div class="form-group">
                                                <label>Department</label>
                                                <select class="form-control" name="department_uuid">

                                                    @foreach(\App\Models\Department::all() as $department)
                                                        <option value="{{$department->uuid}}">{{$department->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Role</label>
                                                <select class="form-control" name="role_uuid">
                                                    @foreach(\App\Models\Role::all() as $role)
                                                        <option value="{{$role->uuid}}">{{$role->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Position</label>
                                                <input class="form-control" type="text" name="position" value="{{$user->position}}">
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input class="form-control" type="email" name="email" value="{{$user->email}}">
                                            </div>
                                            <div class="form-group">
                                                <label>Contact</label>
                                                <input class="form-control" type="text" name="contact" value="{{$user->contact}}">
                                            </div>
                                            <div class="form-group">
                                                <hr>
                                                <input class="form-control" type="hidden" name="password"
                                                       value="{{'Stlm2021'}}">
                                                <button type="submit"
                                                        class="btn btn-primary btn-info-full next-step pull-right">
                                                    Save Details
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('extra-js')
    <script>
        function addMoreFields() {
            var control = '<input class="form-control" type="text" name="dm_fields[]" placeholder="{\'name\':\'text\'}" style="width: 180px;float: left;">';
            $('#fields_wrapper').append(control);
        }
    </script>
@endsection
