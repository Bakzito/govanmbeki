<header class="main-header header-style-one">

    <div class="header-top header-top-one">
        <div class="auto-container">
            <div class="row inner clearfix">
                <div class="col-lg-4 wid_100">
                    <div class="top-left clearfix">
                        <div class="welcome-text">
                            <span class="flag bg_image"
                                  data-image-src="https://stlm.gov.za/wp-content/uploads/2021/01/rsaflag.png"
                                  style="background-image: url(&quot;https://stlm.gov.za/wp-content/uploads/2021/01/rsaflag.png&quot;);"></span>
                            Republic Of South Africa <span class="arrow flaticon-right-arrow-angle"></span></div>
                        <div class="email"><a href="mailto:callcentre@govanmbeki.gov.za"><span
                                        class="icon fa fa-envelope"></span>callcentre@govanmbeki.gov.za</a></div>
                    </div>
                </div>
                <div class="col-lg-4 text-center mid_dp_none">
                    {{--                    <div class="owl-carousel owl-theme single_items owl-loaded owl-drag"><div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s linear 0s;"><div class="owl-item"><div class="mid-text"><p><i class="fa fa-newspaper"></i>The Number One African City(Municipality) in Service Delivery </p></div></div><div class="owl-item"><div class="mid-text"><p><i class="fa fa-newspaper"></i> <a href="https://sacoronavirus.co.za/">SA Corona Virus - Online &amp; News Portal</a></p></div></div><div class="owl-item"><div class="mid-text"><p><i class="fa fa-newspaper"></i> Dear Prospective bidders, Please be aware  alert of scams</p></div></div></div></div><div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span class="icon flaticon-back"></span></button><button type="button" role="presentation" class="owl-next"><span class="icon flaticon-next"></span></button></div><div class="owl-dots disabled"></div></div>--}}
                </div>
                <div class="col-lg-4 wid_100">
                    <div class="top-right clearfix">
                        <div class="phone"><a href="tel:013 249 7000 / 087 285 9010"><span
                                        class="icon fa fa-phone-alt"></span>call on: 013 249 7000 / 087 285 9010</a>
                        </div>
                        <div class="hours">
                            <div class="hours-btn">Open Today:<span class="arrow flaticon-down-arrow"></span></div>
                            <div class="hours-dropdown">
                                <ul>
                                    <li><a href="#">Monday: 07:30 to 16:30</a></li>
                                    <li><a href="#">Tuesday: 07:30 to 16:30</a></li>
                                    <li><a href="#">Wednesday: 07:30 to 16:30</a></li>
                                    <li><a href="#">Thursday: 07:30to 16:30</a></li>
                                    <li><a href="#">Friday: 07:30 to 13:30</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="header-upper">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <!--Logo-->
                <div class="logo-box clearfix">
                    <div class="logo"><a href="https://stlm.gov.za/" class="custom-logo-link" rel="home"
                                         aria-current="page"><img width="600" height="224"
                                                                  src="http://www.govanmbeki.gov.za/wp-content/uploads/2017/04/logo1.png"
                                                                  class="custom-logo" alt="Steve Tshwete"
                                                                  srcset="http://www.govanmbeki.gov.za/wp-content/uploads/2017/04/logo1.png 600w, http://www.govanmbeki.gov.za/wp-content/uploads/2017/04/logo1.png 300w"
                                                                  sizes="(max-width: 600px) 100vw, 600px"></a></div>
                </div>
                <!--Nav-->
                <div class="nav-outer clearfix">
                    <!--Mobile Navigation Toggler-->
                    <div class="mobile-nav-toggler"><span class="icon flaticon-menu-1"></span></div>
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                            <div class="collapse navbar-collapse">
                                <ul id="menu-primary-menu" class="navigation clearfix">

                                    @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'welcome')
                                        <li id="menu-item-783"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-783 nav-item">
                                            <a title="About us" href="https://stlm.gov.za/about-us/"
                                               aria-haspopup="true"
                                               aria-expanded="false" id="783">About us</a>
                                            <ul class="dropdown" aria-labelledby="783" role="menu">
                                                <li id="menu-item-937"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-937 nav-item">
                                                    <a title="Who we are" href="https://stlm.gov.za/about-us/"
                                                       class="dropdown ">Who we are</a></li>
                                                <li id="menu-item-933"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-933 nav-item">
                                                    <a title="History" href="https://stlm.gov.za/history/"
                                                       class="dropdown ">History</a></li>
                                                <li id="menu-item-6481"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6481 nav-item">
                                                    <a title="Achievements" href="https://stlm.gov.za/achievements/"
                                                       class="dropdown ">Achievements</a></li>
                                                <li id="menu-item-8514"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8514 nav-item">
                                                    <a title="Municipality Gallery"
                                                       href="https://stlm.gov.za/municipality-gallery/"
                                                       class="dropdown ">Municipality
                                                        Gallery</a></li>
                                            </ul>
                                            <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                        </li>
                                        <li id="menu-item-10665"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-10665 nav-item">
                                            <a title="Our Structures" href="#" aria-haspopup="true"
                                               aria-expanded="false"
                                               id="10665">Our Structures</a>
                                            <ul class="dropdown" aria-labelledby="10665" role="menu">
                                                <li id="menu-item-2951"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2951 nav-item">
                                                    <a title="Council Structure"
                                                       href="https://stlm.gov.za/council-structure/" class="dropdown ">Council
                                                        Structure</a></li>
                                                <li id="menu-item-3148"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                    <a title="Municipal Structure"
                                                       href="https://stlm.gov.za/municipal-structure/"
                                                       class="dropdown ">Municipal
                                                        Structure</a></li>
                                            </ul>
                                            <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                        </li>
                                        <li id="menu-item-8180"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-8180 nav-item">
                                            <a title="Business" href="#" aria-haspopup="true" aria-expanded="false"
                                               id="8180">Business</a>
                                            <ul class="dropdown" aria-labelledby="8180" role="menu">
                                                <li id="menu-item-12372"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12372 nav-item">
                                                    <a title="Tenders" href="https://stlm.gov.za/tenders/"
                                                       class="dropdown ">Tenders</a></li>
                                                <li id="menu-item-12547"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12547 nav-item">
                                                    <a title="Quotations" href="https://stlm.gov.za/quotations-3/"
                                                       class="dropdown ">Quotations</a></li>
                                                <li id="menu-item-8182"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8182 nav-item">
                                                    <a title="Request for proposals"
                                                       href="https://stlm.gov.za/request-for-proposals/"
                                                       class="dropdown ">Request
                                                        for proposals</a></li>
                                            </ul>
                                            <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                        </li>
                                        <li id="menu-item-11041"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-11041 nav-item">
                                            <a title="Services" href="#" aria-haspopup="true" aria-expanded="false"
                                               id="11041">Services</a>
                                            <ul class="dropdown" aria-labelledby="11041" role="menu">
                                                <li id="menu-item-3101"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3101 nav-item">
                                                    <a title="Municipal Services"
                                                       href="https://stlm.gov.za/municipal-services-2/"
                                                       class="dropdown ">Municipal
                                                        Services</a></li>
                                                <li id="menu-item-3147"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3147 nav-item">
                                                    <a title="Community Services"
                                                       href="https://stlm.gov.za/community-services/" class="dropdown ">Community
                                                        Services</a></li>
                                                <li id="menu-item-13130"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-13130 nav-item">
                                                    <a title="Online Services" href="#" class="dropdown ">Online
                                                        Services</a>
                                                    <ul class="dropdown" aria-labelledby="11041" role="menu">
                                                        <li id="menu-item-13131"
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13131 nav-item">
                                                            <a title="Internet Transacting"
                                                               href="https://www.stlm.gov.za/budget/PP24.html"
                                                               class="dropdown ">Internet Transacting</a></li>
                                                        <li id="menu-item-13132"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13132 nav-item">
                                                            <a title="Account Pins"
                                                               href="https://stlm.gov.za/account-pins/"
                                                               class="dropdown ">Account Pins</a></li>
                                                        <li id="menu-item-13133"
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13133 nav-item">
                                                            <a title="Munsoft e-Service"
                                                               href="https://mymunicipality-mp313.emunsoft.co.za/index.php?r=user/login"
                                                               class="dropdown ">Munsoft e-Service</a></li>
                                                        <li id="menu-item-13133"
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13133 nav-item">
                                                            <a title="Smart City" href="/smart-city"
                                                               class="dropdown ">Smart City</a></li>
                                                    </ul>
                                                    <div class="dropdown-btn"><span class="fa fa-angle-right"></span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                        </li>
                                        <li id="menu-item-784"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784 nav-item">
                                            <a title="Contact" href="https://stlm.gov.za/contact-2/">Contact</a></li>
                                    @else
                                        @if(\Illuminate\Support\Facades\Auth::check())
                                            <li id="menu-item-919"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-937 nav-item">
                                                <a title="Who we are" href="/sub-system/bi-dashboard"
                                                   class="dropdown "><i class="fa fa-chart-bar"></i> Executive Dashboard</a>
                                            </li>
                                            <li id="menu-item-783"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-783 nav-item">
                                                <a title="About us" href="https://stlm.gov.za/about-us/"
                                                   aria-haspopup="true"
                                                   aria-expanded="false" id="783"><i class="fa fa-list"></i> Smart City
                                                    Reports</a>
                                                <ul class="dropdown" aria-labelledby="783" role="menu">
                                                    <li id="menu-item-16823"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-10665 nav-item">
                                                        <a title="Our Structures" href="{{route('current-projects')}}" aria-haspopup="true"
                                                           aria-expanded="false"
                                                           id="10665">Current Projects</a>
                                                        <ul class="dropdown" aria-labelledby="10665" role="menu">
                                                            @foreach(\App\Models\SourceSystem::all() as $sourceSystem)
                                                                <li id="menu-item-2951{{$sourceSystem->id}}"
                                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2951 nav-item">
                                                                    <a title="Council Structure"
                                                                       href="/sub-system/bi-dashboard/{{$sourceSystem->uuid}}"
                                                                       class="dropdown "> {{$sourceSystem->name}}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                    @if(\Illuminate\Support\Facades\Auth::user()->role_uuid == 'ADMIN')
{{--                                                    <li id="menu-item-234"--}}
{{--                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-933 nav-item">--}}
{{--                                                        <a title="Add New Project" href="" class="dropdown" disabled=""--}}
{{--                                                           data-target="#modal_add_new_project" data-toggle="modal">--}}
{{--                                                            Add New Project <small><i class="fa fa-save pull-right"></i>--}}
{{--                                                            </small>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
                                                        @endif
                                                </ul>
                                                <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                            </li>
                                            @if(\Illuminate\Support\Facades\Auth::user()->role_uuid == 'ADMIN')
                                                <li id="menu-item-10665"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-10665 nav-item">
                                                    <a title="Our Structures" href="#" aria-haspopup="true"
                                                       aria-expanded="false"
                                                       id="10665"><i class="fa fa-users"></i> Administration</a>
                                                    <ul class="dropdown" aria-labelledby="10665" role="menu">
                                                        <li id="menu-item-2951"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2951 nav-item">
                                                            <a title="Council Structure"
                                                               href="/bi-dashboard/departments" class="dropdown ">Departments
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-3148"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                            <a title="Municipal Structure"
                                                               href="/bi-dashboard/users"
                                                               class="dropdown "> System Users
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-98273"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                            <a title="Municipal Structure"
                                                               href="/bi-dashboard/settings"
                                                               class="dropdown "> System Logs
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="dropdown-btn"><span class="fa fa-angle-right"></span>
                                                    </div>
                                                </li>
                                            @endif

                                                <li id="menu-item-10665"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-10665 nav-item">
                                                    <a title="Our Structures" href="#" aria-haspopup="true"
                                                       aria-expanded="false"
                                                       id="10665"><i class="fa fa-wrench"></i> Tools</a>
                                                    <ul class="dropdown" aria-labelledby="10665" role="menu">

                                                        @if(\Illuminate\Support\Facades\Auth::user()->role_uuid == 'ADMIN')
                                                        <li id="menu-item-3148"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                            <a title="Municipal Structure"
                                                               href="/sub-system/bi-dashboard/my-tools?name=Calendar&link=https://filesystem.stlm-smartcity.online/index.php/apps/calendar"
                                                               class="dropdown "> Calendar
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-98273"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                            <a title="Municipal Structure"
                                                               href="/sub-system/bi-dashboard/my-tools?name=File%20Manager&link=https://filesystem.stlm-smartcity.online/index.php"
                                                               class="dropdown "> File Manager
                                                            </a>
                                                        </li>
{{--                                                        <li id="menu-item-352342"--}}
{{--                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">--}}
{{--                                                            <a title="Municipal Structure"--}}
{{--                                                               href="/sub-system/bi-dashboard/my-tools?name=Template%20Manager&link=https://filesystem.stlm-smartcity.online/index.php/f/3334"--}}
{{--                                                               class="dropdown "> Templates Manager--}}
{{--                                                            </a>--}}
{{--                                                        </li>--}}
                                                        <li id="menu-item-234234"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                            <a title="Municipal Structure"
                                                               href="/sub-system/bi-dashboard/my-tools?name=Risk%20Analysis&link=https://filesystem.stlm-smartcity.online/index.php/s/LqTe8kmexXziDje"
                                                               class="dropdown "> Risk Analysis
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-78967"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2951 nav-item">
                                                            <a title="Council Structure"
                                                               href="/sub-system/bi-dashboard/my-tools?name=Process%20Flows&link=https://filesystem.stlm-smartcity.online/index.php/apps/files/?dir=/OPEX/TECHNICAL%20TEAM%20REPOSITORY/DATAWAREHOUSE&fileid=280"
                                                               class="dropdown ">
                                                                Data & Process Flows
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-3468732"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                            <a title="Municipal Structure"
                                                               href="/bi-dashboard/settings"
                                                               class="dropdown "> System Monitor
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-675456"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2951 nav-item">
                                                            <a title="Council Structure"
                                                               href="/api/documentation/datawarehouse"
                                                               class="dropdown ">
                                                                Datawarehouse API Documentation
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-675456"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2951 nav-item">
                                                            <a title="Council Structure"
                                                               href="/api/documentation/analytics"
                                                               class="dropdown ">
                                                                Analytics API Documentation
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-3468732"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                            <a title="Municipal Structure"
                                                               href="/sub-system/bi-dashboard/my-tools?name=User%20Manuals&link=https://filesystem.stlm-smartcity.online/index.php/s/zXr8bJPYXJ2HXci"
                                                               class="dropdown "> User Manuals (Training)
                                                            </a>
                                                        </li>
                                                        <li id="menu-item-890354"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                            <a title="Municipal Structure"
                                                               href="/sub-system/bi-dashboard-faqs"
                                                               class="dropdown "> FAQ's
                                                            </a>
                                                        </li>@endif
                                                            <li id="menu-item-234"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-933 nav-item">
                                                                <a title="Workspace Settings" href="" class="dropdown"
                                                                   data-target="#modal_workspace_settings" data-toggle="modal">
                                                                    Workspace Settings <small><i class="fa fa-cog pull-right"></i>
                                                                    </small>
                                                                </a>
                                                            </li>
                                                    </ul>
                                                    <div class="dropdown-btn"><span class="fa fa-angle-right"></span>
                                                    </div>
                                                </li>


                                                <li id="menu-item-919"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-937 nav-item">
                                                    <a title="Who we are" href="/logout"
                                                       class="dropdown "><i class="fa fa-power-off"></i> Logout</a>
                                                </li>

                                        @endif
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>

                <!--Other Links-->
                <div class="other-links clearfix">
                    <!--Language-->
                    <!--Social Links-->
                    <div class="social-links-one">
                        <ul class="clearfix">
                            <li>
                                <button type="button" class="theme-btn search-toggler">
                                    <span class="fa fa-search search-main"></span>
                                    <div class="c-tooltip">
                                        <div class="tooltip-inner">Search STLM BI Dashboards</div>
                                    </div>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--End Header Upper-->

    <!-- Sticky Header  -->
    <div class="sticky-header">
        <div class="auto-container clearfix">
            <!--Logo-->
            <div class="logo pull-left">
                <a href="https://stlm.gov.za/" class="custom-logo-link" rel="home" aria-current="page"><img width="600"
                                                                                                            height="224"
                                                                                                            src="https://stlm.gov.za/wp-content/uploads/2021/02/cropped-stlm-logo600px.png"
                                                                                                            class="custom-logo"
                                                                                                            alt="Steve Tshwete"
                                                                                                            srcset="https://stlm.gov.za/wp-content/uploads/2021/02/cropped-stlm-logo600px.png 600w, https://stlm.gov.za/wp-content/uploads/2021/02/cropped-stlm-logo600px-300x112.png 300w"
                                                                                                            sizes="(max-width: 600px) 100vw, 600px"></a>
            </div>
            <!--Right Col-->
            <div class="pull-right">
                <!-- Main Menu -->
                <nav class="main-menu clearfix">
                    <!--Keep This Empty / Menu will come through Javascript-->

                    <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                        <div class="collapse navbar-collapse">
                            <ul id="menu-primary-menu" class="navigation clearfix">

                                <li id="menu-item-936"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1616 current_page_item active menu-item-936 nav-item">
                                    <a title="Home" href="/" aria-current="page">Home</a></li>

                                @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'welcome')
                                    <li id="menu-item-783"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-783 nav-item">
                                        <a title="About us" href="https://stlm.gov.za/about-us/" aria-haspopup="true"
                                           aria-expanded="false" id="783">About us</a>
                                        <ul class="dropdown" aria-labelledby="783" role="menu">
                                            <li id="menu-item-937"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-937 nav-item">
                                                <a title="Who we are" href="https://stlm.gov.za/about-us/"
                                                   class="dropdown ">Who we are</a></li>
                                            <li id="menu-item-933"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-933 nav-item">
                                                <a title="History" href="https://stlm.gov.za/history/"
                                                   class="dropdown ">History</a>
                                            </li>
                                            <li id="menu-item-6481"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6481 nav-item">
                                                <a title="Achievements" href="https://stlm.gov.za/achievements/"
                                                   class="dropdown ">Achievements</a></li>
                                            <li id="menu-item-8514"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8514 nav-item">
                                                <a title="Municipality Gallery"
                                                   href="https://stlm.gov.za/municipality-gallery/" class="dropdown ">Municipality
                                                    Gallery</a></li>
                                        </ul>
                                        <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                    </li>
                                    <li id="menu-item-10665"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-10665 nav-item">
                                        <a title="Our Structures" href="#" aria-haspopup="true" aria-expanded="false"
                                           id="10665">Our Structures</a>
                                        <ul class="dropdown" aria-labelledby="10665" role="menu">
                                            <li id="menu-item-2951"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2951 nav-item">
                                                <a title="Council Structure"
                                                   href="https://stlm.gov.za/council-structure/"
                                                   class="dropdown ">Council Structure</a></li>
                                            <li id="menu-item-3148"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                <a title="Municipal Structure"
                                                   href="https://stlm.gov.za/municipal-structure/" class="dropdown ">Municipal
                                                    Structure</a></li>
                                        </ul>
                                        <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                    </li>
                                    <li id="menu-item-8180"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-8180 nav-item">
                                        <a title="Business" href="#" aria-haspopup="true" aria-expanded="false"
                                           id="8180">Business</a>
                                        <ul class="dropdown" aria-labelledby="8180" role="menu">
                                            <li id="menu-item-12372"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12372 nav-item">
                                                <a title="Tenders" href="https://stlm.gov.za/tenders/"
                                                   class="dropdown ">Tenders</a>
                                            </li>
                                            <li id="menu-item-12547"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12547 nav-item">
                                                <a title="Quotations" href="https://stlm.gov.za/quotations-3/"
                                                   class="dropdown ">Quotations</a></li>
                                            <li id="menu-item-8182"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8182 nav-item">
                                                <a title="Request for proposals"
                                                   href="https://stlm.gov.za/request-for-proposals/" class="dropdown ">Request
                                                    for proposals</a></li>
                                        </ul>
                                        <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                    </li>
                                    <li id="menu-item-11041"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-11041 nav-item">
                                        <a title="Services" href="#" aria-haspopup="true" aria-expanded="false"
                                           id="11041">Services</a>
                                        <ul class="dropdown" aria-labelledby="11041" role="menu">
                                            <li id="menu-item-3101"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3101 nav-item">
                                                <a title="Municipal Services"
                                                   href="https://stlm.gov.za/municipal-services-2/" class="dropdown ">Municipal
                                                    Services</a></li>
                                            <li id="menu-item-3147"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3147 nav-item">
                                                <a title="Community Services"
                                                   href="https://stlm.gov.za/community-services/"
                                                   class="dropdown ">Community Services</a></li>
                                            <li id="menu-item-13130"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-13130 nav-item">
                                                <a title="Online Services" href="#" class="dropdown ">Online
                                                    Services</a>
                                                <ul class="dropdown" aria-labelledby="11041" role="menu">
                                                    <li id="menu-item-13131"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13131 nav-item">
                                                        <a title="Internet Transacting"
                                                           href="https://www.stlm.gov.za/budget/PP24.html"
                                                           class="dropdown ">Internet Transacting</a></li>
                                                    <li id="menu-item-13132"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13132 nav-item">
                                                        <a title="Account Pins" href="https://stlm.gov.za/account-pins/"
                                                           class="dropdown ">Account Pins</a></li>
                                                    <li id="menu-item-13133"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13133 nav-item">
                                                        <a title="Munsoft e-Service"
                                                           href="https://mymunicipality-mp313.emunsoft.co.za/index.php?r=user/login"
                                                           class="dropdown ">Munsoft e-Service</a></li>
                                                </ul>
                                                <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                            </li>
                                        </ul>
                                        <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                    </li>
                                    <li id="menu-item-784"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784 nav-item">
                                        <a title="Contact" href="https://stlm.gov.za/contact-2/">Contact</a></li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </nav><!-- Main Menu End-->
            </div>
        </div>
    </div><!-- End Sticky Menu -->

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><span class="icon flaticon-targeting-cross"></span></div>

        <nav class="menu-box mCustomScrollbar _mCS_1 mCS_no_scrollbar">
            <div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: 770px;"
                 tabindex="0">
                <div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y"
                     style="position:relative; top:0; left:0;" dir="ltr">
                    <div class="nav-logo"><a href="https://stlm.gov.za/">
                            <img src="https://stlm.gov.za/wp-content/uploads/2021/02/stlm-mobile-logo.png"
                                 alt="mobile logo" class="mCS_img_loaded">
                        </a></div>
                    <div class="menu-outer">
                        <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
                        <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                            <div class="collapse navbar-collapse">
                                <ul id="menu-primary-menu" class="navigation clearfix">
                                    <li id="menu-item-936"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1616 current_page_item active menu-item-936 nav-item">
                                        <a title="Home" href="/" aria-current="page">Home</a>
                                    </li>
                                    @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'welcome')
                                        <li id="menu-item-783"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-783 nav-item">
                                            <a title="About us" href="https://stlm.gov.za/about-us/"
                                               aria-haspopup="true"
                                               aria-expanded="false" id="783">About us</a>
                                            <ul class="dropdown" aria-labelledby="783" role="menu">
                                                <li id="menu-item-937"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-937 nav-item">
                                                    <a title="Who we are" href="https://stlm.gov.za/about-us/"
                                                       class="dropdown ">Who we are</a></li>
                                                <li id="menu-item-933"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-933 nav-item">
                                                    <a title="History" href="https://stlm.gov.za/history/"
                                                       class="dropdown ">History</a></li>
                                                <li id="menu-item-6481"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6481 nav-item">
                                                    <a title="Achievements" href="https://stlm.gov.za/achievements/"
                                                       class="dropdown ">Achievements</a></li>
                                                <li id="menu-item-8514"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8514 nav-item">
                                                    <a title="Municipality Gallery"
                                                       href="https://stlm.gov.za/municipality-gallery/"
                                                       class="dropdown ">Municipality
                                                        Gallery</a></li>
                                            </ul>
                                            <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                        </li>
                                        <li id="menu-item-10665"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-10665 nav-item">
                                            <a title="Our Structures" href="#" aria-haspopup="true"
                                               aria-expanded="false"
                                               id="10665">Our Structures</a>
                                            <ul class="dropdown" aria-labelledby="10665" role="menu">
                                                <li id="menu-item-2951"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2951 nav-item">
                                                    <a title="Council Structure"
                                                       href="https://stlm.gov.za/council-structure/" class="dropdown ">Council
                                                        Structure</a></li>
                                                <li id="menu-item-3148"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3148 nav-item">
                                                    <a title="Municipal Structure"
                                                       href="https://stlm.gov.za/municipal-structure/"
                                                       class="dropdown ">Municipal
                                                        Structure</a></li>
                                            </ul>
                                            <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                        </li>
                                        <li id="menu-item-8180"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-8180 nav-item">
                                            <a title="Business" href="#" aria-haspopup="true" aria-expanded="false"
                                               id="8180">Business</a>
                                            <ul class="dropdown" aria-labelledby="8180" role="menu">
                                                <li id="menu-item-12372"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12372 nav-item">
                                                    <a title="Tenders" href="https://stlm.gov.za/tenders/"
                                                       class="dropdown ">Tenders</a></li>
                                                <li id="menu-item-12547"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12547 nav-item">
                                                    <a title="Quotations" href="https://stlm.gov.za/quotations-3/"
                                                       class="dropdown ">Quotations</a></li>
                                                <li id="menu-item-8182"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8182 nav-item">
                                                    <a title="Request for proposals"
                                                       href="https://stlm.gov.za/request-for-proposals/"
                                                       class="dropdown ">Request
                                                        for proposals</a></li>
                                            </ul>
                                            <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                        </li>
                                        <li id="menu-item-11041"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-11041 nav-item">
                                            <a title="Services" href="#" aria-haspopup="true" aria-expanded="false"
                                               id="11041">Services</a>
                                            <ul class="dropdown" aria-labelledby="11041" role="menu">
                                                <li id="menu-item-3101"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3101 nav-item">
                                                    <a title="Municipal Services"
                                                       href="https://stlm.gov.za/municipal-services-2/"
                                                       class="dropdown ">Municipal
                                                        Services</a></li>
                                                <li id="menu-item-3147"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3147 nav-item">
                                                    <a title="Community Services"
                                                       href="https://stlm.gov.za/community-services/" class="dropdown ">Community
                                                        Services</a></li>
                                                <li id="menu-item-13130"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-13130 nav-item">
                                                    <a title="Online Services" href="#" class="dropdown ">Online
                                                        Services</a>
                                                    <ul class="dropdown" aria-labelledby="11041" role="menu">
                                                        <li id="menu-item-13131"
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13131 nav-item">
                                                            <a title="Internet Transacting"
                                                               href="https://www.stlm.gov.za/budget/PP24.html"
                                                               class="dropdown ">Internet Transacting</a></li>
                                                        <li id="menu-item-13132"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13132 nav-item">
                                                            <a title="Account Pins"
                                                               href="https://stlm.gov.za/account-pins/"
                                                               class="dropdown ">Account Pins</a></li>
                                                        <li id="menu-item-13133"
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13133 nav-item">
                                                            <a title="Munsoft e-Service"
                                                               href="https://mymunicipality-mp313.emunsoft.co.za/index.php?r=user/login"
                                                               class="dropdown ">Munsoft e-Service</a></li>
                                                    </ul>
                                                    <div class="dropdown-btn"><span class="fa fa-angle-right"></span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>
                                        </li>
                                        <li id="menu-item-784"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784 nav-item">
                                            <a title="Contact" href="https://stlm.gov.za/contact-2/">Contact</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--Social Links-->
                    <div class="social-links">
                        <ul class="clearfix">
                            <li><a href="https://www.facebook.com/OfficialSTLM"><span
                                            class="fab fa-facebook-square"></span></a></li>
                            <li><a href="https://www.youtube.com/channel/UCXgWmubIAcL5GJmFkcO4GHw"><span
                                            class="fab fa-youtube"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div id="mCSB_1_scrollbar_vertical"
                     class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical"
                     style="display: none;">
                    <div class="mCSB_draggerContainer">
                        <div id="mCSB_1_dragger_vertical" class="mCSB_dragger"
                             style="position: absolute; min-height: 30px; height: 0px; top: 0px;"
                             oncontextmenu="return false;">
                            <div class="mCSB_dragger_bar" style="line-height: 30px;"></div>
                            <div class="mCSB_draggerRail"></div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div><!-- End Mobile Menu -->
</header>
