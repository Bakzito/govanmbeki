@extends('layouts.app')

@section('extra-css')
    <link href="/css/dashboard-global.css" rel="stylesheet">
@endsection

@section('content')

    <!--FAQs Section-->
    <section class="faqs-section">

        <div class="container-fluid">

            <div class="sec-title with-separator centered">

                <h2>Govan Mbeki Municipality Dashboards <br>How To</h2>

                <div class="separator"><span class="cir c-1"></span><span class="cir c-2"></span><span class="cir c-3"></span></div>

                <div class="lower-text">You have the rights to ask any legal questions, so don’t hestitate to ask us here are listed some questions & answers</div>

            </div>



            <div class="tabs-box faq-tabs">

                <div class="tab-buttons">

                    <div class="row clearfix">

                        <div class="col-lg-2 col-md-4 col-md-4 col-sm-6">

                            <div class="btn-inner tab-btn active-btn" data-tab="#tab-1">

                                <span class="icon flaticon-man"></span>

                                <span class="txt">User Management</span>

                            </div>

                        </div>

                        <div class="col-lg-2 col-md-4 col-md-4 col-sm-6">

                            <div class="btn-inner tab-btn" data-tab="#tab-2">

                                <span class="icon flaticon-sheriff-badge"></span>

                                <span class="txt">Tools</span>

                            </div>

                        </div>

                        <div class="col-lg-2 col-md-4 col-md-4 col-sm-6">

                            <div class="btn-inner tab-btn" data-tab="#tab-3">

                                <span class="icon flaticon-traffic-light"></span>

                                <span class="txt">Integration</span>

                            </div>

                        </div>

                        <div class="col-lg-2 col-md-4 col-md-4 col-sm-6">

                            <div class="btn-inner tab-btn" data-tab="#tab-5">

                                <span class="icon flaticon-museum"></span>

                                <span class="txt">Data Collection</span>

                            </div>

                        </div>

                        <div class="col-lg-2 col-md-4 col-md-4 col-sm-6">

                            <div class="btn-inner tab-btn" data-tab="#tab-6">

                                <span class="icon flaticon-checklist-1"></span>

                                <span class="txt">Data Analysis</span>

                            </div>

                        </div>

                        <div class="col-lg-2 col-md-4 col-md-4 col-sm-6">

                            <div class="btn-inner tab-btn" data-tab="#tab-4">

                                <span class="icon flaticon-statue"></span>

                                <span class="txt">Visual Analytics</span>

                            </div>

                        </div>

                    </div>

                </div>



                <div class="tabs-content">

                    <!--Tab-->

                    <div class="tab active-tab" id="tab-1">

                        <div class="row clearfix">

                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I add Users?
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">Only the system administrator can add new users on the system<br>
                                                        From the user management dashboard, click on “<b>Total number of system users</b>” KPI. <br><br>
                                                        The “Add New User Details” pop up form  will show and you will be requested to supply the new user details.
                                                        <br><br>
                                                        Then click on "<b>submit</b>" to store new user.
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>



                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I integrate an existing System's Reports.
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">You need to login as a Super User and follow the How To User Manuals for Data Warehouse Integration.</div>

                                                </div>

                                            </div>

                                        </div>





                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                    <!--Tab-->

                    <div class="tab" id="tab-2">

                        <div class="row clearfix">

                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I add Users?
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">For adding System users you need to be signed in as a Super User.</div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>



                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I integrate an existing System's Reports.
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">You need to login as a Super User and follow the How To User Manuals for Data Warehouse Integration.</div>

                                                </div>

                                            </div>

                                        </div>





                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                    <!--Tab-->

                    <div class="tab" id="tab-3">

                        <div class="row clearfix">

                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I add Users?
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">For adding System users you need to be signed in as a Super User.</div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>



                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I integrate an existing System's Reports.
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">You need to login as a Super User and follow the How To User Manuals for Data Warehouse Integration.</div>

                                                </div>

                                            </div>

                                        </div>





                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                    <!--Tab-->

                    <div class="tab" id="tab-4">

                        <div class="row clearfix">

                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I add Users?
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">For adding System users you need to be signed in as a Super User.</div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>



                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I integrate an existing System's Reports.
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">You need to login as a Super User and follow the How To User Manuals for Data Warehouse Integration.</div>

                                                </div>

                                            </div>

                                        </div>





                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                    <!--Tab-->

                    <div class="tab" id="tab-5">

                        <div class="row clearfix">

                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I add Users?
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">For adding System users you need to be signed in as a Super User.</div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>



                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I integrate an existing System's Reports.
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">You need to login as a Super User and follow the How To User Manuals for Data Warehouse Integration.</div>

                                                </div>

                                            </div>

                                        </div>





                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                    <!--Tab-->

                    <div class="tab" id="tab-6">

                        <div class="row clearfix">

                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I add Users?
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">For adding System users you need to be signed in as a Super User.</div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>



                            <div class="tab-col col-lg-6 col-md-12 col-sm-12">

                                <div class="inner">

                                    <div class="accordion-box">

                                        <!--Block-->

                                        <div class="accordion block">

                                            <div class="acc-btn">How do I integrate an existing System's Reports.
                                                <div class="icon flaticon-cross"></div>
                                            </div>

                                            <div class="acc-content">

                                                <div class="content">

                                                    <div class="text">You need to login as a Super User and follow the How To User Manuals for Data Warehouse Integration.</div>

                                                </div>

                                            </div>

                                        </div>





                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                </div>

            </div>



        </div>

    </section>

@endsection
