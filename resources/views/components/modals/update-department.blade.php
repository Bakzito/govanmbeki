<!-- Modal -->
<div class="modal fade" id="update_{{$department->uuid}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{route('department-update')}}" method="post" class="async" >
            <input class="form-control" type="hidden" name="uuid" value="{{$department ? $department->uuid : ""}}">

            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <i class="fa fa-plus"></i>Update Department
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container" style="width: 100%;">
                        <div class="form-group">
                            <label>Department REF</label>
                            <input class="form-control" type="text" name="ref" value="{{$department ? $department->ref : ""}}">
                            <label>Name</label>
                            <input class="form-control" type="text" name="name" value="{{$department ? $department->name : ""}}">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input class="form-control" type="text" name="description" value="{{$department ? $department->description : ""}}">

                            <label>Contact</label>
                            <input class="form-control" type="text" name="contact" value="{{$department ? $department->contact : ""}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> update</button>
                </div>
            </div>
        </form>
    </div>
</div>


