<?php

namespace App\Http\Controllers;

use App\Definitions\DataMart;
use App\Definitions\SourceSystem;
use App\Models\Department;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class SourceSystemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application departments dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        try {
            $attributes = $request->all();
            //Update Excel
            if (isset($attributes['import_file']) && $attributes['import_file'] != null) {
                $file = $request->file('import_file');
                $this->uploadFile($file);
            }

            $definition = $this->getSourceSytemDefinition($attributes);
            $sourceSystem = $this->getSourceSytemRepository()->createResource($definition);

            if (isset($sourceSystem->uuid)) {
                $attributes['source_systems_uuid'] = $sourceSystem->uuid;
                $attributes['fields'] = json_encode($attributes['dm_fields']);
                $attributes['name'] = json_encode($attributes['dm_name']);
                $this->createDataMart($sourceSystem->uuid, $attributes);
            }

            return $this->ajaxSuccess('Successfully Added', false, true);
        } catch (QueryException $exception) {
            if (isset($sourceSystem->uuid)) {
                $this->getSourceSytemRepository()->deleteResource($sourceSystem->uuid);
            }

            return $this->ajaxError($exception, false, false);
        }
    }

    public function delete($uuid)
    {
        try {
            $this->getSourceSytemRepository()->deleteResource($uuid);

            return $this->ajaxSuccess('Successfully Deleted', '/sub-system/bi-dashboard/', true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception, false, false);
        }
    }

    private function createDataMart($source_system_uuid, $attributes)
    {
        mkdir('data/' . $attributes['data_path'], 777);
        $definition = $this->getDataMartDefinition($attributes, $source_system_uuid);

        return $this->getDataMartRepository()->createResource($definition);
    }

    private function uploadFile($file)
    {
       ;
        //Display File Name
        echo 'File Name: ' . $file->getClientOriginalName();
        echo '<br>';

        //Display File Extension
        echo 'File Extension: ' . $file->getClientOriginalExtension();
        echo '<br>';

        //Display File Real Path
        echo 'File Real Path: ' . $file->getRealPath();
        echo '<br>';

        //Display File Size
        echo 'File Size: ' . $file->getSize();
        echo '<br>';

        //Display File Mime Type
        echo 'File Mime Type: ' . $file->getMimeType();

        //Move Uploaded File
        $destinationPath = 'uploads';
        $file->move($destinationPath, $file->getClientOriginalName());
    }

    private function getSourceSytemDefinition($data)
    {
        return new SourceSystem($data);
    }

    private function getSourceSytemRepository()
    {
        return new \App\Repositories\SourceSystem();
    }

    private function getDataMartDefinition($data)
    {
        return new DataMart($data);
    }

    private function getDataMartRepository()
    {
        return new \App\Repositories\DataMart();
    }
}
