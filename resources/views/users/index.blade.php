@extends('layouts.app')

@section('extra-css')
    <link href="/css/dashboard-global.css" rel="stylesheet">
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12" style="border: none; padding: 2%;">
            <div class="row clearfix">

                <!--Feature Block-->
                <div class="featured-block-three col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box"><span class="flaticon-briefcase"></span></div>
                            <div class="content">
                                <div class="subtitle">{{\App\Models\Department::all()->count()}}</div>
                                <h4><a href="/bi-dashboard/departments">Total number of Departments</a></h4>
                            </div>
                        </div>
                        <div class="hover-box show">
                            <div class="inner">
                                <h4><a href="/bi-dashboard/departments">{{\App\Models\Department::all()->count()}}</a></h4>
                                <div class="text">Total number of Departments</div>
                            </div>
                        </div>
                        <div class="more-link">
                            <a href="/bi-dashboard/departments"><span class="flaticon-right-2"></span></a>
                        </div>
                    </div>
                </div>

                <!--Feature Block-->
                <div class="featured-block-three col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box"><span class="flaticon-user"></span></div>
                            <div class="content">
                                <div class="subtitle">{{$users->count()}}</div>
                                <h4><a href="/bi-dashboard/users">Total number of System Users</a></h4>
                            </div>
                        </div>
                        <div class="hover-box">
                            <div class="inner">
                                <h4><a href="/bi-dashboard/users">{{\App\User::all()->count()}}</a></h4>
                                <div class="text">Total number of System Users</div>
                            </div>
                        </div>
                        <div class="more-link">
                            <a href="#" data-toggle="modal" data-target="#modal_add_new_user">
                                <span class="flaticon-add-1"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <!--Feature Block-->
                <div class="featured-block-three col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box"><span class="flaticon-bar-chart"></span></div>
                            <div class="content">
                                <div class="subtitle">{{\App\User::all()->count()}}</div>
                                <h4><a href="/bi-dashboard/logs">Total Report Subscriptions</a></h4>
                            </div>
                        </div>
                        <div class="hover-box">
                            <div class="inner">
                                <h4><a href="/bi-dashboard/logs">{{\App\User::all()->count()}}</a></h4>
                                <div class="text">Total Report Subscriptions</div>
                            </div>
                        </div>
                        <div class="more-link">
                            <a href="/bi-dashboard/logs"><span class="flaticon-right-2"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-theme stat-left no-margin no-box-shadow">
                <div class="panel-body bg-theme" style="border: none;padding: 0% 2%;">

                    <table id="datatable_view_users" class="table table-striped table-bordered nowrap" style="width:100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Department</th>
                            <th>Position</th>
                            <th>Email</th>
                            <th>Contact</th>
{{--                            <th>Roles</th>--}}
{{--                            <th>Employee No</th>--}}
{{--                            <th>Digital Signature</th>--}}
                            <th>Date Added</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->surname}}</td>
                                <td>
{{--                                    @php $department = \App\Models\Department::where('uuid', $user->department_uuid)->first(); @endphp--}}
                                    {{$user->department->name ?? ''}}
                                </td>
                                <td>{{$user->position}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->contact}}</td>
{{--                                <td>{{$user->role_uuid}}</td>--}}
{{--                                <td>{{$user->employee_no}}</td>--}}
{{--                                <td>{{$user->digital_signature}}</td>--}}
                                <td>{{$user->date_added}}</td>
                                <td class="centered">
                                    <a class="btn mr-3" href="#" data-toggle="modal"
                                       data-target="#update_{{$user->uuid}}" title="Update User"><i
                                                class="fa fa-edit text-info"></i></a>
                                    <a  class="btn delete" href="/user/delete/{{$user->uuid}}" title="Delete User"><i class="fa fa-trash text-danger"></i></a>
                                </td>
                            </tr>
                        @endforeach
{{--                        @foreach(\App\Utilities\Ldap::index() as $ldapUser)--}}
{{--                            <tr>--}}
{{--                                <td>{{$ldapUser}}</td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
                        </tbody>
                    </table>


                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-sm-8 -->
    </div>

@endsection

@section('extra-js')
    <script>
        $(document).ready(function() {
            $('#datatable_view_users').DataTable( {
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data[0]+' '+data[1];
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                }
            } );
        } );
    </script>
@endsection

