<?php

namespace App\Http\Controllers;

use App\Definitions\SourceSystem;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application departments dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'users' => User::all()
        ];

        return view('users.index', $data);
    }

    public function create(Request $request)
    {
        try {
            $attributes = $request->all();
            $definition = $this->getUserDefinition($attributes);
            $sourceSystem = $this->getUserRepository()->createResource($definition);

            return $this->ajaxSuccess('Successfully Added', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception, false, false);
        }
    }

    public function delete($uuid)
    {
        try {
            $this->getUserRepository()->deleteResource($uuid);

            return $this->ajaxSuccess('Successfully Deleted', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception, false, false);
        }
    }

    public function update(Request $request)
    {
        try {
            $this->getUserRepository()->updateUser($request->all());

            return $this->ajaxSuccess('Successfully Updated', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception, false, false);
        }
    }

    private function getUserDefinition($data)
    {
        return new \App\Definitions\User($data);
    }

    private function getUserRepository()
    {
        return new \App\Repositories\User();
    }
}
