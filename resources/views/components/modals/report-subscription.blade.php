<!-- Modal -->
<div class="modal fade" id="modal_report_subscription_{{$report->uuid}}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-plus"></i> Report Subscription
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container" style="width: 100%;">
                    <div class="row">
                        <section style="width: 100%;">
                            <form class="async" action="/report-subscription" method="post" role="form" id="">
                                @csrf
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <br>
                                        <h2 class="text-center">User Report Subscribe</h2>
                                        <p style="zoom: 0.8">
                                            <br>
                                            @foreach(\App\Models\ReportSubscription::where('report_uuid', $report->uuid)->get() as $subscription)
                                                <button type="button" class="btn btn-xs btn-secondary">
                                                    {{$subscription->user->name ?? ''}} {{$subscription->user->surname ?? ''}}
                                                    <span class="badge">
                                                        <a href="/report/subscription/delete/{{$subscription->uuid}}" class="delete">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </span>
                                                </button>
                                            @endforeach
                                            <hr>
                                        </p>
                                        <div class="form-group" style="zoom: 1.2;padding-left: 5%">
                                            @foreach(\App\Models\User::all() as $user)
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="{{$user->uuid}}" name="user_uuid[]">
                                                    {{$user->name}} {{$user->surname}}
                                                </label>
                                                <br>
                                            @endforeach{{----}}
                                        </div>
                                        <div class="form-group">
                                            <hr>
                                            <input class="form-control" type="hidden" name="report_uuid"
                                                   value="{{$report->uuid}}">
                                            <button type="submit"
                                                    class="btn btn-primary btn-info-full next-step pull-right">
                                                Save Details
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('extra-js')
    <script>
        function addMoreFields() {
            var control = '<input class="form-control" type="text" name="dm_fields[]" placeholder="{\'name\':\'text\'}" style="width: 180px;float: left;">';
            $('#fields_wrapper').append(control);
        }
    </script>
@endsection
