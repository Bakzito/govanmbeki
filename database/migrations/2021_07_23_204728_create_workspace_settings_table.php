<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkspaceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workspace_settings', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('user_uuid')->nullable();
            $table->string('department_uuid')->nullable();
            $table->string('reports')->nullable();
            $table->string('slider')->nullable();
            $table->string('slider_timer')->nullable();
            $table->string('flash_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workspace_settings');
    }
}
