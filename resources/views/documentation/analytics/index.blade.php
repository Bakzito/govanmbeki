@extends('layouts.app')

@section('extra-css')
    <link href="/css/dashboard-global.css" rel="stylesheet">
@endsection

@section('content')

    <div class="sidebar-page-container" style="padding: 0px !important;">
        <iframe src="https://docs.microsoft.com/en-us/power-bi/fundamentals/desktop-getting-started" style="width: 100%;height:800px;"></iframe>
    </div>

@endsection
