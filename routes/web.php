<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Smart City
Route::get('/', function () {
    return view('welcome');
})->name('welcome');
Route::get('/smart-city', function () {
    return view('smart-city.index');
})->name('smart-city-landing');
Route::get('/sub-system/bi-dashboard-faqs', function () {
    return view('faq.index');
})->name('smart-city-faq');
Route::get('/api/v1', function () {
    return 'STLM Smart City - Coptright 2021 | Verion 1.0.0 API (Public / Private - Auth Key Required)';
});

// Auth Routes
Auth::routes();
Route::get('/home', 'BiDashboardController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/user/create', 'UserController@create')->name('user-create');
Route::post('/user/update', 'UserController@update')->name('user-update');
Route::delete('/user/delete/{id}', 'UserController@delete')->name('user-delete');

Route::get('/current-projects','ProjectController@current')->name('current-projects');
//Department Routes
Route::post('/department/create', 'DepartmentController@create')->name('department-create');
Route::delete('/department/delete/{id}', 'DepartmentController@delete')->name('department-delete');
Route::POST('/department/update', 'DepartmentController@update')->name('department-update');


// My Tools
Route::get('/sub-system/bi-dashboard/my-tools', 'BiDashboardController@myTools')->name('sub-system-bi-dashboard-my-tools');

// Documentation
Route::get('/api/documentation/datawarehouse', function () {
    return view('documentation.datawarehouse.index');
})->name('documentation-datawarehouse');
Route::get('/api/documentation/analytics', function () {
    return view('documentation.analytics.index');
})->name('documentation-analytics');


// Executive Dashboard
Route::get('/sub-system/bi-dashboard', 'BiDashboardController@index')->name('sub-system-bi-dashboard-landing');

// Integrated Sub-Systems
Route::get('/sub-system/bi-dashboard/{system}', 'BiDashboardController@subSystem')->name('sub-system-bi-dashboard-subsystem');

// System Administration
Route::get('/bi-dashboard/logs', 'LogController@index')->name('bi-dashboard-logs');
Route::get('/bi-dashboard/users', 'UserController@index')->name('bi-dashboard-users');
Route::get('/bi-dashboard/departments', 'DepartmentController@index')->name('bi-dashboard-departments');
Route::get('/bi-dashboard/settings', 'SettingController@index')->name('bi-dashboard-settings');
Route::get('/sub-system/public-engagement', function () {
    return view('sub-systems.public-engagement.index');
})->name('sub-system-public-engagement');

// Source System Routes
include 'source-system/web.php';

// Reports Routes
include 'reports/web.php';

Route::get('/ldap', function(){

    // make sure your host is the correct one
    // that you issued your secure certificate to
    $ldaphost = "ldaps://ldap.example.com/";

    // Connecting to LDAP
    $ldapconn = ldap_connect($ldaphost)
    or die("That LDAP-URI was not parseable");

});
