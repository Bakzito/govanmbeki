@extends('layouts.app')

@section('extra-css')
    <link href="/css/dashboard-global.css" rel="stylesheet">
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="panel-heading no-border" style="background-color: #0D562A !important;">
                <h3 class="panel-title" style="color: white;">
                    <a href="/smart-city">Smart City</a> / System Logs
                </h3>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="col-md-12" style="    padding: 0.8% 0%;">
            <div class="clearfix"></div>
        </div>

        <!-- KPI's -->
    @include('components.kpi-4',[
    'kpi1_icon' => 'fa fa-cubes','kpi1_label' => 'Total Users','kpi1_value' => 29,
    'kpi2_icon' => 'fa fa-users','kpi2_label' => 'Users Users','kpi2_value' => 1,
    'kpi3_icon' => 'fa fa-user','kpi3_label' => 'Department Heads','kpi3_value' => 29,
    'kpi4_icon' => 'fa fa-cube','kpi4_label' => 'Users Budget','kpi4_value' => 'R '.number_format(0,2)
    ])
    <!-- End KPI's -->

        <div class="col-md-12">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Event Logs</a></li>
                <li><a data-toggle="tab" href="#menu1">Error Logs </a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <!-- Start widget visitor chart -->
                    <div id="tour-10" class="panel stat-stack widget-visitor rounded shadow">
                        <div class="panel-body no-padding br-3">
                            <div class="row row-merge">
                                <div class="col-sm-12 col-md-12">
                                    <div class="panel panel-theme stat-left no-margin no-box-shadow">
                                        <div class="panel-body bg-theme" style="border: none;">

                                            <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>PID</th>
                                                    <th>Date</th>
                                                    <th>Activity</th>
                                                    <th>User</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
{{--                                                @foreach($logs as $log)--}}
{{--                                                    <tr>--}}
{{--                                                        <td>{{$log->id}}</td>--}}
{{--                                                        <td>{{$log->created_at}}</td>--}}
{{--                                                        <td>{{$log->activity}}</td>--}}
{{--                                                        <td>{{$log->user}}</td>--}}
{{--                                                        <td>--}}
{{--                                                            <i class="fa fa-trash pull-right" style="color: red"></i>--}}
{{--                                                        </td>--}}
{{--                                                    </tr>--}}
{{--                                                @endforeach--}}
                                                </tbody>
                                            </table>

                                        </div><!-- /.panel-body -->
                                    </div><!-- /.panel -->
                                </div><!-- /.col-sm-8 -->

                            </div><!-- /.row -->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                    <!--/ End widget visitor chart -->
                </div>
                <div id="menu1" class="tab-pane fade">
                    <!-- Start widget visitor chart -->
                    <div id="tour-10" class="panel stat-stack widget-visitor rounded shadow">
                        <div class="panel-body no-padding br-3">
                            <div class="row row-merge">
                                <div class="col-sm-12 col-md-12">
                                    <div class="panel panel-theme stat-left no-margin no-box-shadow">
                                        <div class="panel-heading no-border" style="background-color: #0D562A !important;">
                                            <div class="pull-left">
                                                <h3 class="panel-title">Error Logs</h3>
                                            </div><!-- /.pull-left -->
                                            <div class="clearfix"></div>
                                        </div><!-- /.panel-heading -->
                                        <div class="panel-body bg-theme" style="border: none;">



                                        </div><!-- /.panel-body -->
                                    </div><!-- /.panel -->
                                </div><!-- /.col-sm-8 -->

                            </div><!-- /.row -->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                    <!--/ End widget visitor chart -->
                </div>

            </div>
        </div><!-- /.row -->
    </div>

@endsection

@section('extra-js')
    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data[0]+' '+data[1];
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                }
            } );
        } );
    </script>
@endsection
