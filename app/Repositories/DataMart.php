<?php

namespace App\Repositories;

use Laravel5Helpers\Repositories\Search;

use App\Models\DataMart as Model;


class DataMart extends Search
{

    protected function getModel()
    {
        return new Model;
    }

    public static function getSourceSystem($source_system_uuid)
    {
        return Model::where('uuid',$source_system_uuid)->first();
    }
}
