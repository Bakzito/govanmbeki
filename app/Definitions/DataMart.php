<?php

namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;
use Illuminate\Support\Str;

class DataMart extends Definition
{
    public $uuid;

    public $source_systems_uuid;

    public $name;

    public $fields;

    public $status;

    public $data_cleaning_method;

    public $data_sync_frequency;

    public $data_sync_day;

    public $data_sync_time;

    public function __construct($data)
    {
        parent::__construct($data);
    }

    protected function setValidators()
    {
        return $this->validators = [
            'source_systems_uuid'        => 'required'
        ];
    }
}
