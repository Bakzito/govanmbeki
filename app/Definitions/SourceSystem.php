<?php

namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;
use Illuminate\Support\Str;

class SourceSystem extends Definition
{
    public $uuid;

    public $name;

    public $description;

    public $ip;

    public $domain;

    public $integration_type;

    public $data_path;

    public function __construct($data)
    {
        parent::__construct($data);
        $this->data_path = str_replace(' ','_', strtolower($this->data_path));
    }

    protected function setValidators()
    {
        return $this->validators = [
            'name'        => 'required'
        ];
    }
}
