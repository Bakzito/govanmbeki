@extends('layouts.app')

@section('extra-css')
    <link href="/css/dashboard-global.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="content-side col-lg-12 col-md-12 col-sm-12">
            <div class="content-inner">
                <div class="service-details">
                    <div class="two-col">
                        <div class="row clearfix">

                            <div class="text-col col-lg-3 col-md-3 col-sm-12">
                                <aside class="sidebar">
                                    <div class="bg-layer">
                                        <div class="image-layer"
                                             style="background-image:url(images/background/sidebar-bg-image.jpg);"></div>
                                    </div>
                                    <div class="sidebar-widget services-widget">
                                        <div class="widget-content">
                                            <ul class="links clearfix">
                                                <li>
                                                        <a href="{{ URL::previous() }}">
                                                            <span class="icon fa fa-angle-left"></span>
                                                            <span class="ttl">Back</span>
                                                        </a>
                                                    </li>
                                                @foreach($sourceSystem->reports->where('published', 1) as $report)
                                                    <li>
                                                        <a data-toggle="tab" href="#report-{{$report->uuid}}">
                                                            <span class="icon fa fa-angle-right"></span>
                                                            <span class="ttl">{{ucwords($report->title ?? $report->name)}}</span>
                                                        </a>
                                                    </li>
                                                @endforeach
                                                @if(\Illuminate\Support\Facades\Auth::user()->role_uuid == 'ADMIN')
{{--                                                    <li style="position: fixed;bottom: 5%">--}}
{{--                                                        <a data-toggle="modal" data-target="#modal_add_new_report" href="#">--}}
{{--                                                            <span class="icon fa fa-angle-left"></span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
                                                    <li style="position: fixed;bottom: 0%">
                                                        <a data-toggle="modal" data-target="#modal_add_new_report" href="#">
                                                            <span class="icon fa fa-plus"></span>
                                                        </a>
                                                    </li>

                                                <li>
                                                    <a data-toggle="pill" href="#config">
                                                        <span class="icon fa fa-cog"></span>
                                                        <span class="ttl">Workspace Settings</span>
                                                    </a>
                                                </li>
                                                    @endif
                                            </ul>
                                        </div>
                                    </div>
                                </aside>
                            </div>

                            <div class="image-col col-lg-9 col-md-9 col-sm-12 ">

                                <div class="tab-content">

                                    <div id="config" class="tab-pane fade">
                                        <!-- Start widget visitor chart -->
                                        <div id="tour-10" class="panel stat-stack widget-visitor rounded shadow">
                                            <div class="panel-body no-padding br-3">
                                                <div class="row row-merge">
                                                    <div class="col-sm-12 col-md-12">
                                                        <div class="panel panel-theme stat-left no-margin no-box-shadow">
                                                            <div class="panel-body bg-theme"
                                                                 style="background-color: white !important;border: none;">
                                                                @include('settings.components.settings-tabs')
                                                            </div><!-- /.panel-body -->
                                                        </div><!-- /.panel -->
                                                    </div><!-- /.col-sm-8 -->

                                                </div><!-- /.row -->
                                            </div><!-- /.panel-body -->
                                        </div><!-- /.panel -->
                                        <!--/ End widget visitor chart -->
                                    </div>

                                    @php $show = 'in active' @endphp
                                    @foreach($sourceSystem->reports->where('published', 1) as $report)
                                        <div id="report_{{$report->uuid}}" class="tab-pane {{$show}} fade">
                                            <!-- Start widget visitor chart -->
                                            <div id="tour-10" class="panel stat-stack widget-visitor rounded shadow">
                                                <div class="panel-body no-padding br-3">
                                                    <div class="row row-merge">
                                                        <div class="col-sm-12 col-md-12">
                                                            <div class="panel panel-theme stat-left no-margin no-box-shadow">
                                                                <div class="panel-body bg-theme"
                                                                     style="background-color: white !important;border: none;">
                                                                    <iframe style="width: 100%;zoom: 1.3;" height="600" src="{{$report->embed}}" frameborder="0" allowFullScreen="true"></iframe>
                                                                </div><!-- /.panel-body -->
                                                            </div><!-- /.panel -->
                                                        </div><!-- /.col-sm-8 -->

                                                    </div><!-- /.row -->
                                                </div><!-- /.panel-body -->
                                            </div><!-- /.panel -->
                                            <!--/ End widget visitor chart -->
                                        </div>
                                        @php $show = ''@endphp
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
