<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataMartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_marts', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('source_systems_uuid')->nullable();
            $table->string('name')->nullable();
            $table->string('fields')->nullable();
            $table->string('status')->nullable();
            $table->string('data_cleaning_method')->nullable();
            $table->string('data_sync_frequency')->nullable();
            $table->string('data_sync_day')->nullable();
            $table->string('data_sync_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_marts');
    }
}
