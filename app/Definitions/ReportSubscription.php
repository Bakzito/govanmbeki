<?php

namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;
use Illuminate\Support\Str;

class ReportSubscription extends Definition
{
    public $uuid;

    public $user_uuid;

    public $department_uuid;

    public $unit_uuid;

    public $report_uuid;

    public function __construct($data)
    {
        parent::__construct($data);
    }

    protected function setValidators()
    {
        return $this->validators = [
            'report_uuid'        => 'required'
        ];
    }
}
