<?php

namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;

class WorkspaceSettings extends Definition
{
    public $uuid;

    public $user_uuid;

    public $department_uuid;

    public $reports;

    public $slider;

    public $slider_timer;

    public $flash_message;

    public function __construct($data)
    {
        parent::__construct($data);
    }

    protected function setValidators()
    {
        return $this->validators = [
            'user_uuid' => 'required'
        ];
    }
}
