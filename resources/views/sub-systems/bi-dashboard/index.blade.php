@extends('layouts.app')

@section('extra-css')
    <style>
        .services-widget .links li a {
            position: relative;
            display: block;
            padding: 28px 0px 22px;
            line-height: 30px;
            /*padding-left: 80px;*/
            min-height: 80px;
            color: #222222;
            font-weight: 700;
            box-shadow: 0px 25px 30px 0px rgb(0 0 0 / 7%);
            transition: all 500ms ease;
            -moz-transition: all 500ms ease;
            -webkit-transition: all 500ms ease;
            -ms-transition: all 500ms ease;
            -o-transition: all 500ms ease;
        }

        .services-widget .links li {
            position: relative;
            display: block;
            font-size: 17px;
            color: #222222;
            text-transform: capitalize;
            font-family: "Manjari", sans-serif;
            border-bottom: 1px solid rgba(0, 0, 0, 0.07);
        }

        .services-widget .links {
            position: relative;
            background: #ffffff;
            box-shadow: 0px 0px 25px 0px rgb(0 0 0 / 10%);
            text-align: center;
        }

        h5 {
            display: inline-block;
            padding: 10px;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }

        .p {
            text-align: center;
            padding-top: 40px;
            font-size: 13px;
        }
    </style>
    <style>
        section {
            color: white;
            font-family: helvetica;
            text-transform: uppercase;
            overflow-x: hidden;
            zoom: 0.8;
        }

        section div {
            display: flex;
            flex-wrap: nowrap;
            white-space: nowrap;
            min-width: 100%;
        }

        section div .news-message {
            display: flex;
            flex-shrink: 0;
            height: 45px;
            align-items: center;
            animation: slide-left 20s linear infinite;
            font-size: 7px;
        }

        section div .news-message p {
            font-size: 2.5em;
            font-weight: 100;
            padding-left: 0.5em;
        }

        @keyframes slide-left {
            from {
                -webkit-transform: translateX(0);
                transform: translateX(0);
            }
            to {
                -webkit-transform: translateX(-100%);
                transform: translateX(-100%);
            }
        }
        .carousel-control-next, .carousel-control-prev {
            width: 2% !important;
            background-color: #f4f4f4 !important;
        }
    </style>
@endsection

@section('content')

    @include('components.news-flash')

    <div class="row">
        <div class="col-md-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    @php $active = 'active'; @endphp
                    @foreach($sliderReports as $sliderReport)

                        @php $sliderReport = str_replace('"','',$sliderReport); @endphp
                        @php $sliderReport = str_replace('[','',$sliderReport); @endphp
                        @php $sliderReport = str_replace(']','',$sliderReport); @endphp
                        @php $report = \App\Models\Report::where('uuid', $sliderReport)->first() @endphp

                        @if(isset($report->embed))
                            <div class="carousel-item {{$active}}">
                                <iframe style="width: 100%;zoom: 1.2;" height="600" src="{{$report->embed}}"
                                        frameborder="0"
                                        allowFullScreen="true"></iframe>
                            </div>
                            @php $active = ''; @endphp
                        @endif

                    @endforeach
                </div>
                <!-- Controls -->
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    {{--                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    {{--                    <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    {{--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
    <script type="text/javascript" src="https://unpkg.com/popper.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    {{--    <script type="text/javascript" src="https://unpkg.com/popper.js"></script>--}}
    <script>
        $('.carousel').carousel({
            interval: 60000,
            pause: "false"
        });
    </script>
@endsection
