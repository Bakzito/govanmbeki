<?php

namespace App\Utilities;

use App\Models\Client;

class SmsSender {

    private $url;
    private $username;
    private $password;
    public $recipient;
    public $message;

    public function __construct($recepient = '', $message = '') {
        $this->url = "http://api.bulksms.com/v1/messages";
        $this->username = "dash99";
        $this->password = "Authentication";
        $this->recipient = $recepient;
        $this->message = $message;
    }

    public function send() {

        $messages = array(array('to' => $this->recipient, 'body' => $this->message),);
        $post_body = json_encode($messages);
        $ch = curl_init();

        $headers = array(
            'Content-Type:application/json',
            'Authorization:Basic ' . base64_encode("$this->username:$this->password")
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);  // Allow cUrl functions 20 seconds to execute
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  // Wait 10 seconds while trying to connect

        $output = array();
        $output['server_response'] = curl_exec($ch);
        $curl_info = curl_getinfo($ch);
        $output['http_status'] = $curl_info['http_code'];
        $output['error'] = curl_error($ch);

        curl_close($ch);
        $result = $output;

        return $result;
    }

    public static function getAllMessages() {
        $ch = curl_init();
        $headers = array(
            'Content-Type:application/json',
            'Authorization:Basic ' . base64_encode("dash99:Authentication")
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, 'http://api.bulksms.com/v1/messages');
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);  // Allow cUrl functions 20 seconds to execute
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  // Wait 10 seconds while trying to connect

        return curl_exec($ch);
    }

    public static function getUserName($cellphone){
        return Client::where('cellphone', $cellphone)->first();
    }

}
