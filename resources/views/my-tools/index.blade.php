@extends('layouts.app')

@section('extra-css')
    <style>
        .sidebar-page-container .sidebar {
            padding-left: 0px !important;
        }
        .sidebar .cat-links li a:before {
            content: none !important;
        }
        .sidebar-page-container {
            padding: 30px 0px 80px !important;
        }
        .sidebar .sidebar-widget {
            margin-bottom: 5px !important;
        }
    </style>
@endsection

@section('content')

    <div class="sidebar-page-container" style="padding: 0px !important;">
        <iframe src="{{$link}}" style="width: 100%;height:800px;margin-top: -3%;"></iframe>
    </div>

@endsection
