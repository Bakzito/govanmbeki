@extends('layouts.app')

@section('extra-css')
    <style>
        .sidebar-page-container .sidebar {
            padding-left: 0px !important;
        }

        .sidebar .cat-links li a:before {
            content: none !important;
        }

        .sidebar-page-container {
            padding: 30px 0px 80px !important;
        }

        .sidebar .sidebar-widget {
            margin-bottom: 5px !important;
        }

        html {
            overflow-y: scroll;
        }

    </style>
@endsection

@section('content')

    <section class="highlights-section-two" style="height: inherit;">

        <div class="image-layer" style="background-image:url(theme/images/smart-4308821_1920.jpg);"></div>

        <div class="auto-container">
            <div class="sec-title  with-separator centered">
                <br><br>
                <h2 style="color:white">Smart City Projects</h2>
                <div class="separator"><span class="cir c-1"></span><span class="cir c-2"></span><span class="cir c-3"></span></div>
                <div class="more-link"><a href="/"><span class="flaticon-left-2"></span></a></div>
            </div>
            <div class="row clearfix">
                <div class="featured-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box">
                                <i aria-hidden="true" class="icon  flaticon-like"></i>						</div>
                            <div class="content">
                                <h4><a href="" target="_blank" rel="nofollow">Current <br>Projects</a></h4>
                                <div class="text">Smart City Project Current Projects</div>
                                <div style="width: 125%;height: 180px;  overflow: none; margin: -10%;">
                                    <div class=""  style="padding: 10% 1%;">
                                        <a href="/sub-system/bi-dashboard" target="_blank" rel="nofollow">
                                            Govan Mbeki Municipality Dashboards <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="https://pe.stlm-smartcity.online/index.php?r=space%2Fspace%2Fhome&cguid=56962c65-0fcc-400d-9a05-98ff16ac9a6b" target="_blank" rel="nofollow">
                                             Public Engagement  <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class=""  style="padding: 10% 1%;">
                                        <a href="https://stlm-scm.irsglobal.net" target="_blank" rel="nofollow">
                                            E-Procurement <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="featured-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box">
                                <i aria-hidden="true" class="icon  flaticon-password"></i>
                            </div>

                            <div class="content">
                                <h4><a href="" target="_blank" rel="nofollow">Coming Soon Projects</a></h4>
                                <div class="text">STLM Smart City Projects coming soon  </div>

                                <div style="width: 125%;height: 180px;  overflow: auto; margin: -10%;">
                                    <div class=""  style="padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Capital Projects Management <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            E-Recruitment  <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class=""  style="padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Contract Management <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            E-Wayleave Management <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Facility Management  <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            E facilities management
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Call Center <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Renewable Energy  <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Smart Meters and Vending Systems <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="" style="    padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Audio Visual CMS
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="featured-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="content-box">
                            <div class="icon-box">
                                <i aria-hidden="true" class="icon  flaticon-settings"></i>
                            </div>
                            <div class="content">
                                <h4><a href="https://mymunicipality-mp313.emunsoft.co.za/index.php?r=user/login" target="_blank" rel="nofollow">Other <br>Projects</a>
                                </h4>
                                <div class="text"> Other STLM Projects to be Awarded.</div>
                                <div style="width: 125%;height: 180px;  overflow: none; margin: -10%;">
                                    <div class=""  style="padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Workflow Management System <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class=""  style="padding: 10% 1%;">
                                        <a href="#" target="_blank" rel="nofollow">
                                            Workforce Management System<i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>

@endsection
