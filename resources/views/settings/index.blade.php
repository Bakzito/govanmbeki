@extends('layouts.app')

@section('extra-css')
    <link href="/css/dashboard-global.css" rel="stylesheet">
@endsection

@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-theme stat-left no-margin no-box-shadow">

                <iframe style="width: 100%;zoom: 1.3;" height="600" src="https://stlm-smartcity.online/server/monitoring-tools/" frameborder="0" allowFullScreen="true"></iframe>

            </div><!-- /.panel -->
        </div><!-- /.col-sm-8 -->
    </div>

@endsection

@section('extra-js')
    <script>
        $(document).ready(function() {
            $('#datatable_source_systems').DataTable();
        } );
    </script>
    @endsection
