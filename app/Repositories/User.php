<?php

namespace App\Repositories;

use Laravel5Helpers\Repositories\Search;

use App\Models\User as Model;


class User extends Search
{

    protected function getModel()
    {
        return new Model;
    }

    public function updateUser($user)
    {
        $user_obj= Model::where('uuid',$user['user_id'])->first();

        $user_obj->name=$user['name'];
        $user_obj->surname=$user['surname'];
        $user_obj->department_uuid=$user['department_uuid'];
        $user_obj->employee_no = null;
        $user_obj->contact = $user['contact'];
        $user_obj->role_uuid = null;
        $user_obj->digital_signature = null;

        $user_obj->save();
        return $user;
    }
}
