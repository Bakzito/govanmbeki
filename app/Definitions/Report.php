<?php

namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;

class Report extends Definition
{
    public $uuid;

    public $source_systems_uuid;

    public $department_uuid;

    public $name;

    public $title;

    public $description;

    public $embed_type;

    public $embed;

    public $refresh_rate;

    public $published;

    public function __construct($data)
    {
        parent::__construct($data);
    }

    protected function setValidators()
    {
        return $this->validators = [
            'source_systems_uuid' => 'required'
        ];
    }
}
