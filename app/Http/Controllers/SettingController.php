<?php

namespace App\Http\Controllers;

use App\Definitions\WorkspaceSettings;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application settings dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('settings.index');
    }

    /**
     * Show the application departments dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        try {
            $attributes = $request->all();
            $attributes['reports'] = json_encode($attributes['reports']);
            $definition = $this->getReportDefinition($attributes);
            $existingWorkspaceSettings = \App\Models\WorkspaceSettings::where('user_uuid',$attributes['user_uuid']);

            if($existingWorkspaceSettings->first()){
                $existingWorkspaceSettings->update([
                    'slider' => $attributes['slider'],
                    'slider_timer' => $attributes['slider_timer'],
                    'reports' => (string)$attributes['reports'],
                    'flash_message' => $attributes['flash_message']
                ]);

                return $this->ajaxSuccess('Successfully Updated', false, true);
            }

            $report = $this->getReportRepository()->createResource($definition);

            return $this->ajaxSuccess('Successfully Created', false, true);
        } catch (QueryException $exception) {
            return $this->ajaxError($exception->getMessage(), false,false);
        }
    }

    private function getReportDefinition($data){
        return new WorkspaceSettings($data);
    }

    private function getReportRepository(){
        return new \App\Repositories\WorkdspaceSettings();
    }
}
