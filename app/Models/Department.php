<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class Department extends UuidModel
{
    protected $table = 'departments';

    public function users()
    {
        return $this->hasMany('App\Models\User', 'department_uuid', 'uuid');
    }

    public function procurementPlans()
    {
        return $this->hasMany('App\Models\ProcurementPlan', 'department_uuid', 'uuid');
    }

    public function requisitionPlans()
    {
        return $this->hasMany('App\Models\RequisitionPlan', 'department_uuid', 'uuid');
    }
}
