<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->

<!-- START @HEAD -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
    <!-- START @META SECTION -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>DASHBOARD | Steve-Tshwete</title>
    <!--/ END META SECTION -->

    <!-- START @FAVICONS -->
    <link href="/theme-admin/ico/icon.png" rel="shortcut icon">
    <!--/ END FAVICONS -->

    <!-- START @FONT STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
    <!--/ END FONT STYLES -->

    <!-- START @GLOBAL MANDATORY STYLES -->
    <link href="/theme-admin/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!--/ END GLOBAL MANDATORY STYLES -->

    <!-- START @PAGE LEVEL STYLES -->
    <link href="/theme-admin/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/animate.css/animate.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="/theme-admin/plugins/jquery.gritter/css/jquery.gritter.css" rel="stylesheet">
    <link href="/theme-admin/plugins/bootstrap-tour/bootstrap-tour.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/theme-admin/plugins/chartist/chartist.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/cube-portfolio/cubeportfolio.min.css" rel="stylesheet">
    <link href="/theme-admin/plugins/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <link href="/theme-admin/plugins/c3js-chart/c3.min.css" rel="stylesheet">
    <!--/ END PAGE LEVEL STYLES -->

    <!-- START @THEME STYLES -->
    <link href="/theme-admin/css/reset.css" rel="stylesheet">
    <link href="/theme-admin/css/layout.css" rel="stylesheet">
    <link href="/theme-admin/css/components.css" rel="stylesheet">
    <link href="/theme-admin/css/plugins.css" rel="stylesheet">
    <link href="/theme-admin/css/default.theme.css" rel="stylesheet" id="theme">
    <link href="/theme-admin/css/custom.css" rel="stylesheet">
    <link href="/theme-admin/css/opex.css" rel="stylesheet">
    <!--/ END THEME STYLES -->

    <script src="/theme-admin/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/opexdash2017/js/jquery.cookie.min.js"></script>

    <!-- DATA TABLES CSS -->
    <link href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.8/css/responsive.bootstrap4.min.css" rel="stylesheet">

    <!-- TOASTR / AJAX -->
    <link href="={{asset('vendors/premium-digital/ajax/toastr/toastr.min.css')}}" rel="stylesheet">

    <style>
        html, body {
            height: 100% !important;
        }

        .sidebar-circle .sidebar-menu > li > ul > li.dash::after {
            background-color: green !important;
        }

        .sidebar-circle .sidebar-menu > li > ul > li.dash2::after {
            background-color: white !important;
        }

        .sidebar-circle .sidebar-menu > li > ul > li.danger::after {
            background-color: red !important;
        }

        a {
            color: #ffffff;
        }

        .bg-theme {
            background-color: #ffffff !important;
            border: 1px solid #ffffff;
            color: black;
        }
    </style>

    <!-- START @IE SUPPORT -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../../assets/global/plugins/bower_components/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../assets/global/plugins/bower_components/respond-minmax/dest/respond.min.js"></script>
    <![endif]-->
    <!--/ END IE SUPPORT -->
</head>
<!--/ END HEAD -->

<!--

|=========================================================================================================================|
|  TABLE OF CONTENTS (Use search to find needed section)                                                                  |
|=========================================================================================================================|
|  01. @HEAD                        |  Container for all the head elements                                                |
|  02. @META SECTION                |  The meta tag provides metadata about the HTML document                             |
|  03. @FAVICONS                    |  Short for favorite icon, shortcut icon, website icon, tab icon or bookmark icon    |
|  04. @FONT STYLES                 |  Font from google fonts                                                             |
|  05. @GLOBAL MANDATORY STYLES     |  The main 3rd party plugins css file                                                |
|  06. @PAGE LEVEL STYLES           |  Specific 3rd party plugins css file                                                |
|  07. @THEME STYLES                |  The main theme css file                                                            |
|  08. @IE SUPPORT                  |  IE support of HTML5 elements and media queries                                     |
|=========================================================================================================================|
|  09. @BODY                        |  Contains all the contents of an HTML document                                      |
|  10. @WRAPPER                     |  Wrapping page section                                                              |
|  11. @HEADER                      |  Header page section contains about logo, top navigation, notification menu         |
|  12. @SIDEBAR LEFT                |  Sidebar page section contains all sidebar menu left                                |
|  13. @PAGE CONTENT                |  Contents page section contains breadcrumb, content page, footer page               |
|  14. @SIDEBAR RIGHT               |  Sidebar page section contains all sidebar menu right                               |
|  15. @BACK TOP                    |  Element back to top and action                                                     |
|=========================================================================================================================|
|  16. @CORE PLUGINS                |  The main 3rd party plugins script file                                             |
|  17. @PAGE LEVEL PLUGINS          |  Specific 3rd party plugins script file                                             |
|  18. @PAGE LEVEL SCRIPTS          |  The main theme script file                                                         |
|=========================================================================================================================|

START @BODY
|=========================================================================================================================|
|  TABLE OF CONTENTS (Apply to body class)                                                                                |
|=========================================================================================================================|
|  01. page-boxed                   |  Page into the box is not full width screen                                         |
|  02. page-header-fixed            |  Header element become fixed position                                               |
|  03. page-sidebar-fixed           |  Sidebar element become fixed position with scroll support                          |
|  04. page-sidebar-minimize        |  Sidebar element become minimize style width sidebar                                |
|  05. page-footer-fixed            |  Footer element become fixed position with scroll support on page content           |
|  06. page-sound                   |  For playing sounds on user actions and page events                                 |
|=========================================================================================================================|

-->
<body class="page-session page-sound page-header-fixed page-sidebar-fixed demo-dashboard-session">

<!--[if lt IE 9]>
<p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a
        href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- START @WRAPPER -->
<section id="wrapper">

    <!-- START @HEADER -->
    <header id="header">

        <!-- Start header left -->
        <div class="header-left">
            <!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
            <div class="navbar-minimize-mobile left">
                <i class="fa fa-bars"></i>
            </div>
            <!--/ End offcanvas left -->

            <!-- Start navbar header -->
            <div class="navbar-header" style="border-bottom: none; background-color: #000; padding: 1px;">

                <!-- Start brand -->
{{--                <a id="tour-1" class="navbar-brand" href="./" style="background: #000; ">--}}
{{--                <img src="/img/logo.png" style="width: 100%;">--}}
{{--                </a>--}}
                <!-- /.navbar-brand -->
                <!--/ End brand -->

            </div><!-- /.navbar-header -->
            <!--/ End navbar header -->

            <!-- Start offcanvas right: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
            <div class="navbar-minimize-mobile right">
                <i class="fa fa-cog"></i>
            </div>
            <!--/ End offcanvas right -->

            <div class="clearfix"></div>
        </div><!-- /.header-left -->
        <!--/ End header left -->

        <!-- Start header right -->
        <div class="header-right">
            <!-- Start navbar toolbar -->
            <div class="navbar navbar-toolbar">

                <!-- Start left navigation -->
                <ul class="nav navbar-nav navbar-left">

                    <!-- Start sidebar shrink -->
                    <li id="tour-2" class="navbar-minimize">
                        <a href="javascript:void(0);" title="Minimize sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                    <!--/ End sidebar shrink -->

                    <!-- Start form search -->

                    <!--/ End form search -->

                </ul><!-- /.nav navbar-nav navbar-left -->
                <!--/ End left navigation -->

                <!-- Start right navigation -->
                <ul class="nav navbar-nav navbar-right"><!-- /.nav navbar-nav navbar-right -->

                    <!-- Start messages -->
                    <li id="tour-4" class="dropdown navbar-message">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope-o"></i><span
                                    class="count label label-danger rounded">0</span></a>

                        <!-- Start dropdown menu -->
                        <div class="dropdown-menu animated flipInX">
                            <div class="dropdown-header">
                                <span class="title">Messages <strong>(0)</strong></span>
                                <span class="option text-right"><a href="#">+ New message</a></span>
                            </div>
                            <div class="dropdown-body">

                                <!-- Start message search -->
                                <form class="form-horizontal" action="#">
                                    <div class="form-group has-feedback has-feedback-sm m-5">
                                        <input type="text" class="form-control input-sm"
                                               placeholder="Search message...">
                                        <button type="submit"
                                                class="btn btn-theme fa fa-search form-control-feedback"></button>
                                    </div>
                                </form>
                                <!--/ End message search -->

                                <!-- Start message list -->
                                <div class="media-list niceScroll">


                                </div>
                                <!--/ End message list -->

                            </div>
                            <div class="dropdown-footer">

                            </div>
                        </div>
                        <!--/ End dropdown menu -->

                    </li><!-- /.dropdown navbar-message -->
                    <!--/ End messages -->


                    <!-- Start profile -->
                    <li id="tour-6" class="dropdown navbar-profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="meta">
                                    <span class="avatar"><img src="/theme-admin/img/user.png" class="img-circle"
                                                              alt="admin"></span>
                                    <span class="text hidden-xs hidden-sm text-muted">User</span>
                                    <span class="caret"></span>
                                </span>
                        </a>
                        <!-- Start dropdown menu -->
                        <ul class="dropdown-menu animated flipInX">
                            {{--                            <li class="dropdown-header">Account</li>--}}
                            {{--                            <li><a href="#"><i class="fa fa-user"></i>View profile</a></li>--}}
                            <li class="divider"></li>
                            <li><a href="/logout"><i class="fa fa-sign-out"></i>Logout</a></li>
                        </ul>
                        <!--/ End dropdown menu -->
                    </li><!-- /.dropdown navbar-profile -->
                    <!--/ End profile -->

                    <!-- Start settings -->
                    <!--<li id="tour-7" class="navbar-setting pull-right">
                        <a href="javascript:void(0);"><i class="fa fa-cog fa-spin"></i></a>
                    </li><!-- /.navbar-setting pull-right -->
                    <!--/ End settings -->

                </ul>
                <!--/ End right navigation -->

            </div><!-- /.navbar-toolbar -->
            <!--/ End navbar toolbar -->
        </div><!-- /.header-right -->
        <!--/ End header left -->

    </header> <!-- /#header -->
    <!--/ END HEADER -->

    <!--

    START @SIDEBAR LEFT
    |=========================================================================================================================|
    |  TABLE OF CONTENTS (Apply to sidebar left class)                                                                        |
    |=========================================================================================================================|
    |  01. sidebar-box               |  Variant style sidebar left with box icon                                              |
    |  02. sidebar-rounded           |  Variant style sidebar left with rounded icon                                          |
    |  03. sidebar-circle            |  Variant style sidebar left with circle icon                                           |
    |=========================================================================================================================|

    -->
    <aside id="sidebar-left" class="sidebar-circle">

        <!-- Start left navigation - profile shortcut -->
        <div id="tour-8" class="sidebar-content">
            <a id="tour-1" class="navbar-brand" href="/" style="background: white;float: none;    padding: 0;">
                <img src="/img/logo.png" style="border: none;border-radius: 0%;margin: 0% 0% 0% 30%;">
            </a>
        </div>
        <!-- /.sidebar-content -->
        <!--/ End left navigation -  profile shortcut -->

        <!-- Start left navigation - menu -->
        <ul id="tour-9" class="sidebar-menu">

            <li class="submenu active">
                <a href="/sub-system/bi-dashboard">
                    <span class="icon"><i class="fa fa-home"></i></span>
                    <span class="text">Executive Dashboard</span>
                    <span class="arrow"></span>
                    <span class="selected"></span>
                </a>
            </li>


            <li class="submenu">
                <a href="javascript:void(0);">
                    <span class="icon"><i class="fa fa-list-alt"></i></span>
                    <span class="text">Smart City Reports</span> &nbsp;
                    <span class="arrow"></span>
                </a>
                <ul>
                    <li class="submenu">
                        <a href="javascript:void(0);">
                            <span class="icon"><i class="fa fa-briefcase"></i> &nbsp; </span>
                            <span class="text">Current Projects</span>
                            <span class="arrow"></span>
                        </a>
                        <ul>
                            @foreach(\App\Models\SourceSystem::all() as $sourceSystem)
                                <li class="dash">
                                    <a href="/sub-system/bi-dashboard/{{strtolower($sourceSystem->name)}}">
                                        <span class="" style="color: green">{{$sourceSystem->name}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
{{--                    @if(\Illuminate\Support\Facades\Auth::user()->role_uuid == 'ADMIN')--}}
{{--                            <a href="javascript:$('#modal_add_new_project').modal('toggle');" disabled="true">--}}
{{--                                <span class="icon"><i class="fa fa-plus"></i> &nbsp; </span>--}}
{{--                                <span class="text">Add New Project</span>--}}
{{--                            </a>--}}
{{--                        @endif--}}
                    </li>
                </ul>
            </li>

            @if(\Illuminate\Support\Facades\Auth::user()->role_uuid == 'ADMIN')
            <li class="submenu">
                <a href="javascript:void(0);">
                    <span class="icon"><i class="fa fa-users"></i></span>
                    <span class="text">Administration</span> &nbsp;
                    <span class="arrow"></span>
                </a>
                <ul>
                    <li><a href="/bi-dashboard/departments">Departments</a></li>
                    <li><a href="/bi-dashboard/users">System Users</a></li>
                    <li><a href="/bi-dashboard/logs">System Logs</a></li>
                </ul>
            </li>
            <li class="submenu ">
                <a href="javascript:void(0);">
                    <span class="icon"><i class="fa fa-wrench"></i></span>
                    <span class="text">My Tools</span>  &nbsp;
                    <span class="arrow"></span>
                </a>
                <ul>
                    <li>
                        <a href="/sub-system/bi-dashboard/my-tools?name=Inbox&link=https://filesystem.stlm-smartcity.online/index.php/apps/mail/">Inbox</a>
                    </li>
                    <li>
                        <a href="/sub-system/bi-dashboard/my-tools?name=Calendar&link=https://filesystem.stlm-smartcity.online/index.php/apps/calendar/dayGridMonth/now">Calendar</a>
                    </li>
                    <li>
                        <a href="/sub-system/bi-dashboard/my-tools?name=File Manager&link=https://filesystem.stlm-smartcity.online/index.php">File
                            Manager</a></li>

                    <li><a href="/sub-system/bi-dashboard/my-tools?name=How To&link=">How To</a></li>
{{--                    <li>--}}
{{--                        <a href="/sub-system/bi-dashboard/my-tools?name=Template Manager&link=https://filesystem.stlm-smartcity.online/index.php/f/3334">Template--}}
{{--                            Manager</a></li>--}}
{{--                    <li>--}}
                        <a href="/sub-system/bi-dashboard/my-tools?name=Risk Analysis&link=https://filesystem.stlm-smartcity.online/index.php/s/LqTe8kmexXziDje">Risk
                            Analysis</a></li>
                    <li>
                        <a href="/sub-system/bi-dashboard/my-tools?name=Process Flows&link=https://filesystem.stlm-smartcity.online/index.php/f/373">Data
                            & Process Flows</a></li>
                    <li>
                        <a href="/sub-system/bi-dashboard/my-tools?name=Help Desk&link=https://hd.next-iot.online/login.php?get_started">Help
                            Desk</a></li>
                    <li><a href="/bi-dashboard/settings">Settings</a></li>
                    <li><a href="#">FAQ</a></li>
                </ul>
            </li>
            <li class="submenu hidden">
                <a href="javascript:void(0);">
                    <span class="icon"><i class="fa fa-star"></i></span>
                    <span class="text">Customer Satisfaction</span>
                    <span class="arrow"></span>
                </a>
                <ul>

                    <!--<li><a href="/webapps/ccc">Customer Care</a></li>-->
                    <!--<li><a href="/webapps/crm">Customer Relations</a>li>-->
                    <li><a href="https://menlynpark.irsglobal.net/nps/home">Net Promoter</a></li>
                    <li><a href="https://hd.next-iot.online/login.php?get_started">IT Help Desk</a></li>
                    <!--<li><a href="/webapps/survey">Customer Survey</a></li>-->
                    <!--<li><a href="https://pas.irsglobal.net/admin/user/login?u=info@irsglobal.net&p=golive3000">Perception Audit</a></li>-->

                </ul>
            </li>
            <li class="submenu hidden">
                <a href="javascript:void(0)">
                    <span class="icon"><i class="fa fa-wrench"></i></span>
                    <span class="text">Operational Tools</span>
                    <span class="arrow"></span>
                </a>
                <ul>
                    <!--<li><a href="javascript:void(0)" style="color: red;">Control Centre</a></li>-->
                    <li><a href="/webapps/audit">System Audit</a></li>
                    <li><a href="http://opex.irsglobal.net/rms">Resolution Management</a></li>
                    <!--<li><a href="https://ipms.irsglobal.net/">Program Charter</a></li>-->
                    <li><a href="/webapps/parking_validation">Parking Validation</a></li>
                    <!--<li><a href="javascript:void(0)" style="color: red;">Parking Loop System</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">Visitor Management</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">Access Control</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">Fire Detection</a></li>-->
                    <!--<li><a href="https://sim.irsglobal.net">CCTV</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">Building Management System</a></li>-->
                    <li><a href="http://steve-tshwete.irsglobal.net/legaro/html/ltr/menu/">Energy Management</a></li>
                    <!--<li><a href="javascript:void(0)" style="color: red;">Facility Management</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">Fibre Connectivity</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">WIFI & Bluetooth</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">Asset management</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">Lease management</a></li>-->
                    <!--<li><a href="javascript:void(0)" style="color: red;">Digital Audio Visual</a></li>-->
                    <!--<li><a href="https://ess.irsglobal.net/" style="color:red;">Panic Alarm System</a></li> -->
                    <li><a href="https://sim.irsglobal.net/users/login?u=info@irsglobal.net&p=golive3000">Security
                            Incident</a></li>
                    <!--<li><a href="/webapps/audit">System Audit</a></li>
                    <li><a href="/webapps/nonconformities">Nonconformity</a></li>
                    <li><a href="http://www.i-rms.co.za/Steve-Tshwete/">Resolution Management</a></li>
                    <li><a href="/webapps/program_charter">Program Charter</a></li>
                    <li><a href="http://businessanalytics.co.za/Steve-Tshwete/web/">File Manager</a></li>
                    <li><a href="/webapps/parking_validation">LPR Validation</a></li>-->
                </ul>
            </li>
            <li class="submenu hidden">
                <a href="javascript:void(0);">
                    <span class="icon"><i class="fa fa-gift"></i></span>
                    <span class="text">Marketing tools</span>
                    <span class="arrow"></span>
                </a>
                <ul>
                    <li><a href="https://dms.irsglobal.net/index.php">Digital Marketing Store</a></li>
                    <li><a href="https://digitalglass.irsglobal.net">Live Video CMS</a></li>
                    <li><a href="https://demo.irsglobal.net/people_counting/demo?type=d">People Counting</a></li>
                    <li><a href="/webapps/amr">Demographic Data</a></li>
                </ul>
            </li>
            <li class="submenu hidden">
                <a href="javascript:void(0);">
                    <span class="icon"><i class="fa fa-home"></i></span>
                    <span class="text">Demo Building</span>
                    <span class="arrow open fa-angle-double-down"></span>
                </a>
                <ul>
                    <li class=""><a href="https://demo.irsglobal.net/people_counting/demo?type=st" target="_blank">People
                            Counting</a></li>
                    <li class=""><a href="https://demo.irsglobal.net/people_counting/reports/heatmaps?type=st"
                                    target="_blank">Heatmaps</a></li>
                    <li class=""><a href="https://frd.irsglobal.net/95" target="_blank">Demographics</a></li>
                </ul>

            </li>
            <li class="hidden">
                <a href="./../msi">
                    <span class="icon"><i class="fa fa-stack-exchange"></i></span>
                    <span class="text">Master Systems Int.</span>
                </a>
            </li>
            <li class="hidden">
                <a href="https://ndmweb.next-iot.online/">
                    <span class="icon"><i class="fa fa-etsy"></i></span>
                    <span class="text">E-Procurement</span>
                </a>
            </li>
            @endif
        </ul>
        <!--/ End left navigation - menu -->

        <!-- Start left navigation - footer -->
        <div id="tour-10" class="sidebar-footer hidden-xs hidden-sm hidden-md">
            <a id="fullscreen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
               data-title="Fullscreen"><i class="fa fa-desktop"></i></a>
            <a id="lock-screen" data-url="/logout" class="pull-left" href="javascript:void(0);" data-toggle="tooltip"
               data-placement="top" data-title="Lock Screen"><i class="fa fa-lock"></i></a>
            <a id="logout" data-url="/logout" class="pull-left" href="javascript:void(0);" data-toggle="tooltip"
               data-placement="top" data-title="Logout"><i class="fa fa-power-off"></i></a>
        </div><!-- /.sidebar-footer -->
        <!--/ End left navigation - footer -->

    </aside><!-- /#sidebar-left -->
    <!--/ END SIDEBAR LEFT -->

    <!-- START @PAGE CONTENT -->
    <section id="page-content">

        <!-- Start body content -->
        <div class="body-content animated fadeIn" style="min-height: 1000px;padding: 20px !important;">

            @yield('content')

        </div><!-- /.body-content -->
        <!--/ End body content -->

        <!-- Start footer content -->
        <footer class="footer-content fixed-bottom" style="position: fixed;
    bottom:0%;width:100%; ">
                    <span id="tour-19">
                        2015 - <span id="copyright-year"></span> &copy; OPEX Business Solutions.
                    </span>
        </footer><!-- /.footer-content -->
        <!--/ End footer content -->

    </section><!-- /#page-content -->
    <!--/ END PAGE CONTENT -->


</section><!-- /#wrapper -->
<!--/ END WRAPPER -->

<!-- START @BACK TOP -->
<div id="back-top" class="animated pulse circle">
    <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->

<!-- START @ADDITIONAL ELEMENT -->
<div class="modal modal-success fade" id="modal-bootstrap-tour" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Welcome to Rustenburg Rapid Transport Dashboard</h4>
            </div>
            <div class="modal-body">
                <div class="media">
                    <div class="media-left" style="padding-right: 15px;">
                        <a href="#">
                            <img data-no-retina class="media-object" src="/theme-admin/img/mascot.jpg" alt="..."
                                 style="width: 100px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Hello, I am the OPEX Mascot</h4>
                        <b>Introduction</b> - Rustenburg Rapid Transport dashboard contains an overview of all the tools
                        integrated together, it includes a project management application that facilitates project
                        tasks, assignments, responsibilities and completion including an integration with the intranet
                        to access all the right documentation all in one dashboard.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="BlankonDashboard.callModal2()"
                        data-dismiss="modal">Let's tour <i class="fa fa-arrow-circle-right"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-bootstrap-tour-new-features" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">For Training and guidance</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>For training, guidance and questions relating to the use and posibilities of the RRT Dashboard
                        please contact OPEX Business Solutions</p>
                    <p>We may proceed with the tour</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="BlankonDashboard.handleTour()"
                        data-dismiss="modal">Next
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" id="modal-bootstrap-tour-end" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">End RRT Dashboard Tour</h4>
            </div>
            <div class="modal-body">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img data-no-retina class="media-object" src="/theme-admin/img/mascot.jpg" alt="..."
                                 style="width: 100px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Thanks for watching!</h4>
                        <p>Thank you for view our RRT Tour tour. We hope it has been of much help :</p>
                        <!--<ul class="list-inline">
                            <li>
                                <a href="https://wrapbootstrap.com/user/djavaui" class="btn btn-inverse tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Wrapbootstrap">W</a>
                            </li>
                            <li>
                                <a href="http://djavaui.com" class="btn btn-lilac tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Our Website"><i class="fa fa-globe"></i></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/djavaui/" class="btn btn-facebook tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Facebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/djavaui" class="btn btn-twitter tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Twitter"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/102744122511959250698" class="btn btn-googleplus tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Google+"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="https://github.com/djavaui" class="btn btn-default tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Github"><i class="fa fa-github"></i></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCt_dudJF4_0bOkQkwYN2qQQ" class="btn btn-youtube tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Youtube"><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>-->
                        <b>Thanks so much!</b>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="BlankonDashboard.handleTour()"
                        data-dismiss="modal">Let's tour again <i class="fa fa-arrow-circle-right"></i></button>
            </div>
        </div>
    </div>
</div>
<!--/ END ADDITIONAL ELEMENT -->

<div class="modal modal-success fade" id="modal-coming-soon" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Coming Soon</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>All short codes and USSD campaigns for promotions and additional revenue generation will be
                        visible on this dashboard categorized
                        by location, campaign, promotion, cost, revenue and people catchment</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-mgr1" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Point of Sale Integration</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>This solution now has the capability to integrate to all back end point-of-sale servers
                        and all meta data on transactional values will be uploaded to this dashboard.
                        No individual or personal information will be captured and stored. </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-mgr2" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Lease Management Integration</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>This solution now is part of the tenant portal where online lease management
                        and billing will be executed. The solutions has a two way resolution platform
                        which eliminates a significant part of administration.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-mgr3" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Asset Management and Tracking</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>Every asset from electrical, mechanical, electronic and physical marketing hardware
                        for visual display will be tagged via RFID technology. The front-end of the dashboard
                        will relate the geo-location of these assets as well as the depreciating value in
                        real-time.</p>
                    <p>The dashboard will relate service intervals and operational maintenance and relate
                        that as a field service alert to the management.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-mgr4" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Revenue Generation from Applications</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>Free WiFi Accessibility via The mobile application allows customers and tenants
                        to view notifications, promotions and marketing materials prior to accessing the internet.
                        Revenue is generated from advertising on this platform. More information can be seen on the
                        digital marketing store platform.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-mgr5" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Retailer Performance Management</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>The dashboard will relate the overall performance of a tenant and this will include turnover,
                        marketing initiatives and customer satisfaction. </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-mgr6" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Carbon footprint reduction solution</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>The dashboard will relate the actual carbon footprint of the building in
                        real-time and key activities on-going for reduction.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-mgr7" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Parking Capacity Planning</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>The dashboard will corelate people arriving via vehicles or foot, as well</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-mgr8" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">VVIP Portal</h4>
            </div>
            <div class="modal-body" style="height: 200px; overflow: scroll;">
                <div class="table-responsive">
                    <p>This portal will allow important customers to pre-book their parking, restaurant, shopping
                        experience
                        <br>This will be integrated to a mobile application and communication will be seen on the
                        customer care centre</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- GLOBAL MODALS -->
@include('components.modals.add-project')
@include('components.modals.add-report', ['source_system_uuid'=>$sourceSystem->uuid ?? null])
<!-- END GLOBAL MODALS -->

<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
<!-- START @CORE PLUGINS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="/theme-admin/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="/theme-admin/plugins/bootstrap/bootstrap.min.js"></script>
<script src="/theme-admin/plugins/typehead.js/handlebars.js"></script>
<script src="/theme-admin/plugins/typehead.js/typeahead.bundle.min.js"></script>
<script src="/theme-admin/plugins/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script src="/theme-admin/plugins/jquery.sparkline.min/index.js"></script>
<script src="/theme-admin/plugins/jquery-easing-original/jquery.easing.1.3.min.js"></script>
<script src="/theme-admin/plugins/ionsound/ion.sound.min.js"></script>
<script src="/theme-admin/plugins/bootbox/bootbox.js"></script>
<script src="/theme-admin/plugins/retina.js/retina.min.js"></script>
<!--/ END CORE PLUGINS -->

<!-- START @PAGE LEVEL PLUGINS -->
<script src="/theme-admin/plugins/bootstrap-session-timeout/bootstrap-session-timeout.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.spline.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.categories.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.resize.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.pie.js"></script>
<script src="/theme-admin/plugins/dropzone/dropzone.min.js"></script>
<script src="/theme-admin/plugins/jquery.gritter/js/jquery.gritter.min.js"></script>
<script src="/theme-admin/plugins/skycons-html5/skycons.js"></script>
<script src="/theme-admin/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="/theme-admin/plugins/counter-up/jquery.counterup.min.js"></script>
<script src="/theme-admin/plugins/bootstrap-tour/bootstrap-tour.min.js"></script>
<script src="/theme-admin/plugins/moment/moment.min.js"></script>
<script src="/theme-admin/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/theme-admin/plugins/chartist/chartist.min.js"></script>
<script src="/theme-admin/plugins/cube-portfolio/jquery.cubeportfolio.min.js"></script>
<script src="/theme-admin/plugins/d3/d3.min.js" charset="utf-8"></script>
<script src="/theme-admin/plugins/c3js-chart/c3.min.js"></script>
<script src="/theme-admin/plugins/flot/excanvas.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.time.js"></script>
<script src="/theme-admin/plugins/flot/jshashtable-2.1.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.symbol.js"></script>
<script src="/theme-admin/plugins/jquery-numberformatter/jquery.numberformatter-1.2.3.min.js"></script>
<script src="/theme-admin/plugins/flot/jquery.flot.axislabels.js"></script>
<script src="/theme-admin/plugins/chartjs/Chart.min.js"></script>
<script src="/theme-admin/plugins/raphael/raphael-min.js"></script>
<script src="/theme-admin/plugins/morrisjs/morris.min.js"></script>

<!-- DATA TABLES -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/responsive/2.2.8/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/responsive/2.2.8/js/responsive.bootstrap4.min.js"></script>
<!--/ END PAGE LEVEL PLUGINS -->


<!-- START @PAGE LEVEL SCRIPTS -->
<script src="/theme-admin/js/apps.js"></script>

<!-- AJAX -->
<script type="text/javascript" src="{{asset('vendors/premium-digital/ajax/toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/premium-digital/ajax/bootbox.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/premium-digital/ajax/ajax-form.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/premium-digital/ajax/main.js')}}"></script>

<script>
    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        });
        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
</script>

@yield('extra-js')

</body>

<!--/ END BODY -->

</html>
